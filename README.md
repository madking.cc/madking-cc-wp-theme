# Responsive simple Wordpress Theme

![Screencap showing theme](https://madking.cc/wp-content/uploads/screenshot.png)

## Functions

* Responsive Bootstrap Layout
* Top, Main, Footer Menu with Offcanvas and Dropdown Items
* Widget: Hierarchical Taxonomy Navigation
* Font, Background, Color adjustments with customizer
* jQuery Support

## Live

[madking.cc](https://madking.cc)
