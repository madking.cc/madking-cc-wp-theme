function mcc_show_submenu (el) {
  el.find('.dropdown-menu').toggleClass('show')
}

function mcc_hide_submenu (el) {
  el.find('.dropdown-menu').removeClass('show')
}

$(document).ready(function () {

  wp.customize('submenu_keep_first_open_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {

        $('.navbar-nav').
          each(
            function () { mcc_show_submenu($(this).find('.dropdown:first'))}
          )
      }
      else {

        $('.navbar-nav').
          each(
            function () { mcc_hide_submenu($(this).find('.dropdown:first'))}
          )
      }

    })
  })

})

// Thanks to https://stackoverflow.com/a/21648508
function hexToRgbA (hex, opacity) {
  var c
  if (/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)) {
    c = hex.substring(1).split('')
    if (c.length == 3) {
      c = [c[0], c[0], c[1], c[1], c[2], c[2]]
    }
    c = '0x' + c.join('')
    return 'rgba(' + [(c >> 16) & 255, (c >> 8) & 255, c & 255].join(',') +
      ',' + opacity + ')'
  }
  throw new Error('Bad Hex')
}

/**
 * This file adds some LIVE to the Theme Customizer live preview. To leverage
 * this, set your custom settings to 'postMessage' and then add your handling
 * here. Your javascript should grab settings from customizer controls, and
 * then make any necessary changes to the page using jQuery.
 */
(function ($) {

    //
    wp.customize('article_meta_top_show_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {
                $('.entry-meta').css('display', 'block')
            }
            else {
                $('.entry-meta').css('display', 'none')
            }

        })
    })
    //
    wp.customize('article_thumbnail_header_show_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {
                $('.article-thumbnail-header').css('display', 'block')
            }
            else {
                $('.article-thumbnail-header').css('display', 'none')
            }

        })
    })
    //
    wp.customize('article_preview_thumbnail_header_show_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {
                $('#article-preview-thumbnail').css('display', 'block')
                $('.entry-col').css('width', '100%')
                $('.entry-col').css('max-width', '100%')
            }
            else {
                $('#article-preview-thumbnail').css('display', 'none')
            }

        })
    })




  // Meta Top/Bottom Display
  wp.customize('showhide_metatop_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#meta-top-wrapper').css('display', 'block')
      }
      else {
        $('#meta-top-wrapper').css('display', 'none')
      }

    })
  })

  // Header Display
  wp.customize('showhide_header_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('.header-wrapper').css('display', 'flex')
      }
      else {
        $('.header-wrapper').css('display', 'none')
      }

    })
  })

  // Meta Main Display
  wp.customize('showhide_metamain_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#meta-main-wrapper').css('display', 'block')
      }
      else {
        $('#meta-main-wrapper').css('display', 'none')
      }

    })
  })

  // Content Display
  wp.customize('showhide_content_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#content-wrapper').css('display', 'block')
      }
      else {
        $('#content-wrapper').css('display', 'none')
      }

    })
  })

  // Meta Bottom Display
  wp.customize('showhide_metabottom_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#meta-bottom-wrapper').css('display', 'block')
      }
      else {
        $('#meta-bottom-wrapper').css('display', 'none')
      }

    })
  })

  // Footer Display
  wp.customize('showhide_footer_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#footer-wrapper').css('display', 'block')
      }
      else {
        $('#footer-wrapper').css('display', 'none')
      }

    })
  })

  // Meta Top/Bottom Background Color
  wp.customize('background_color_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('background_color_opacity_meta_top_setting').
        get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-top-wrapper').css('background-color', rgba)
      $('#meta-bottom-wrapper').css('background-color', rgba)
    })
  })

  // Meta Top/Bottom Background Color Opacity
  wp.customize('background_color_opacity_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('background_color_meta_top_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#meta-top-wrapper').css('background-color', rgba)
      $('#meta-bottom-wrapper').css('background-color', rgba)
    })
  })

  // Meta Top/Bottom Background Cover
  wp.customize('background_image_cover_meta_top_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#meta-top-wrapper').css('background-size', 'cover')
        $('#meta-bottom-wrapper').css('background-size', 'cover')
      }
      else {
        $('#meta-top-wrapper').css('background-size', 'auto')
        $('#meta-bottom-wrapper').css('background-size', 'auto')
      }

    })
  })

  // Meta Top/Bottom Border Color
  wp.customize('border_color_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('border_color_opacity_meta_top_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-top-wrapper').css('border-bottom', '1px solid ' + rgba)

      $('#meta-top-search-wrapper').css('border-right', '1px solid ' + rgba)
      $('#meta-top-search-wrapper').css('border-left', '1px solid ' + rgba)
      $('#meta-top-search-wrapper form .search_button_wrapper').
        css('border-left', '1px solid ' + rgba)

      $('#meta-bottom-wrapper').css('border-bottom', '1px solid ' + rgba)
    })
  })

  // Meta Top/Bottom Border Opacity
  wp.customize('border_color_opacity_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('border_color_meta_top_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#meta-top-wrapper').css('border-bottom', '1px solid ' + rgba)
      $('#meta-top-search-wrapper').css('border-left', '1px solid ' + rgba)
      $('#meta-top-search-wrapper').css('border-right', '1px solid ' + rgba)
      $('#meta-top-search-wrapper form .search_button_wrapper ').
        css('border-left', '1px solid ' + rgba)
      $('#meta-bottom-wrapper').css('border-bottom', '1px solid ' + rgba)
    })
  })

  // Meta Top/Bottom Submenu Background Color
  wp.customize('submenu_background_color_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize(
        'submenu_background_color_opacity_meta_top_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-top-wrapper .dropdown-menu').css('background-color', rgba)
      $('#meta-bottom-wrapper .dropdown-menu').css('background-color', rgba)
    })
  })

  // Meta Top/Bottom Submenu Background Color Opacity
  wp.customize('submenu_background_color_opacity_meta_top_setting',
    function (value) {
      value.bind(function (newval) {

        var color = wp.customize('submenu_background_color_meta_top_setting').
          get()
        var rgba = hexToRgbA(color, newval)

        $('#meta-top-wrapper .dropdown-menu').css('background-color', rgba)
        $('#meta-bottom-wrapper .dropdown-menu').css('background-color', rgba)
      })
    })

  // Meta Top/Bottom Submenu Background Image Cover
  wp.customize('submenu_background_image_cover_meta_top_setting',
    function (value) {
      value.bind(function (newval) {

        if (newval == 1) {
          $('#meta-top-wrapper .dropdown-menu').css('background-size', 'cover')
        }
        else {
          $('#meta-bottom-wrapper .dropdown-menu').
            css('background-size', 'auto')
        }

      })
    })

  // Meta Top/Bottom Submenu Border Color
  wp.customize('submenu_border_color_meta_top_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize(
        'submenu_border_color_opacity_meta_top_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-top-wrapper .dropdown-menu').css('border', '1px solid ' + rgba)
      $('#meta-bottom-wrapper .dropdown-menu').
        css('border', '1px solid ' + rgba)
    })
  })

  // Meta Top/Bottom Submenu Border Color Opacity
  wp.customize('submenu_border_color_opacity_meta_top_setting',
    function (value) {
      value.bind(function (newval) {

        var color = wp.customize('submenu_border_color_meta_top_setting').get()
        var rgba = hexToRgbA(color, newval)

        $('#meta-top-wrapper .dropdown-menu').
          css('border', '1px solid ' + rgba)
        $('#meta-bottom-wrapper .dropdown-menu').
          css('border', '1px solid ' + rgba)
      })
    })

  // Meta Top/Bottom Submenu Background Hover Color
  wp.customize('submenu_background_hover_color_meta_top_setting',
    function (value) {
      value.bind(function (newval) {

        $('#meta-top-wrapper .dropdown-menu a').hover(function () {
            var opacity = wp.customize(
              'submenu_background_hover_color_opacity_meta_top_setting').get()
            var rgba = hexToRgbA(newval, opacity)

            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )

        $('#meta-bottom-wrapper .dropdown-menu a').hover(function () {
            var opacity = wp.customize(
              'submenu_background_hover_color_opacity_meta_top_setting').get()
            var rgba = hexToRgbA(newval, opacity)

            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )

      })
    })

  // Meta Top/Bottom Submenu Background Hover Color Opacity
  wp.customize('submenu_background_hover_color_opacity_meta_top_setting',
    function (value) {
      value.bind(function (newval) {

        $('#meta-top-wrapper .dropdown-menu a').hover(function () {
            var color = wp.customize(
              'submenu_background_hover_color_meta_top_setting').get()
            var rgba = hexToRgbA(color, newval)
            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )

        $('#meta-bottom-wrapper .dropdown-menu a').hover(function () {
            var color = wp.customize(
              'submenu_background_hover_color_meta_top_setting').get()
            var rgba = hexToRgbA(color, newval)
            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )
      })
    })

    //
    wp.customize('header_center_content_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {
                $('.entry-title').css('text-align', 'center')
                $('.entry-meta').css('text-align', 'center')
            }
            else {
                $('.entry-title').css('text-align', 'left')
                $('.entry-meta').css('text-align', 'left')
            }

        })
    })
    //
    wp.customize('header_page_center_content_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {
                $('.page-title').css('text-align', 'center')
            }
            else {
                $('.page-title').css('text-align', 'left')
            }

        })
    })
    //
    wp.customize('content_center_content_setting', function (value) {
        value.bind(function (newval) {

            if (newval == 1) {

                    $('.entry-content').css('text-align', 'center')
                    $('.entry-footer').css('text-align', 'center')
            }
            else {

                        $('.entry-content').css('text-align', 'left')
                        $('.entry-footer').css('text-align', 'left')
            }

        })
    })





  // Meta Top/Bottom font Color
  wp.customize('font_color_meta_top_setting', function (value) {
    value.bind(function (newval) {
      $('#meta-top-wrapper a').css('color', newval)
      $('#meta-bottom-wrapper a').css('color', newval)


      $('#meta-top-search-wrapper form .search_button_wrapper .fa').css('color', newval)
      $('#meta-top-search-wrapper form input[type="search"]').css('color', newval)
    })
  })

  // Meta Top/Bottom font Color Hover
  wp.customize('font_color_hover_meta_top_setting', function (value) {
    value.bind(function (newval) {

      $('#meta-top-wrapper ul .nav-link').hover(function () {

          $(this).css('color', newval)
        },
        function () {
          var color = wp.customize(
            'font_color_meta_top_setting').get()
        $(this).css('color', color)}
      )
      $('#meta-bottom-wrapper ul .nav-link').hover(function () {

          $(this).css('color', newval)
        },
        function () {     var color = wp.customize(
          'font_color_meta_top_setting').get()
          $(this).css('color', color)}
      )

    })
  })

  // Header font Color
  wp.customize('font_color_header_setting', function (value) {
    value.bind(function (newval) {
      $('.header-wrapper').css('color', newval)
    })
  })

  // Header font Color Link
  wp.customize('font_color_link_header_setting', function (value) {
    value.bind(function (newval) {
      $('.header-wrapper a').css('color', newval)
    })
  })

  // Headerfont Color Hover
  wp.customize('font_color_hover_header_setting', function (value) {
    value.bind(function (newval) {

      $('.header-wrapper a').hover(function () {

          $(this).css('color', newval)
        },
        function () { var color = wp.customize(
          'font_color_link_header_setting').get()
          $(this).css('color', color)}
      )
      $('.header-wrapper a').hover(function () {

          $(this).css('color', newval)
        },
        function () {    var color = wp.customize(
          'font_color_link_header_setting').get()
          $(this).css('color', color)}
      )

    })
  })

  // Meta Main font Color
  wp.customize('font_color_meta_main_setting', function (value) {
    value.bind(function (newval) {
      $('#meta-main-wrapper a').css('color', newval)
    })
  })

  // Meta Main font Color Hover
  wp.customize('font_color_hover_meta_main_setting', function (value) {
    value.bind(function (newval) {

      $('#meta-main-wrapper a').hover(function () {
          $(this).css('color', newval)
        },
        function () {
          var color = wp.customize(
            'font_color_meta_main_setting').get()
          $(this).css('color', color)
        }
      )
      $('#meta-main-wrapper a').hover(function () {
          $(this).css('color', newval)
        },
        function () {
          var color = wp.customize(
            'font_color_meta_main_setting').get()
          $(this).css('color', color)
        }
      )
    })
  })

  // Footer font Color
  wp.customize('font_color_footer_setting', function (value) {
    value.bind(function (newval) {
      $('#footer-wrapper').css('color', newval)
    })
  })

  // Footer font Color Link
  wp.customize('font_color_link_footer_setting', function (value) {
    value.bind(function (newval) {
      $('#footer-wrapper a').css('color', newval)
    })
  })

  // Footer font Color Hover
  wp.customize('font_color_hover_footer_setting', function (value) {
    value.bind(function (newval) {


      $('#footer-wrapper a').hover(function () {

          $(this).css('color', newval)
        },
        function () {           var color = wp.customize(
          'font_color_link_footer_setting').get()
          $(this).css('color', color)}
      )
    })
  })

  // Content font Color
  wp.customize('font_color_content_setting', function (value) {
    value.bind(function (newval) {
      $('#content-wrapper').css('color', newval)
    })
  })

  // Content font Color Link
  wp.customize('font_color_link_content_setting', function (value) {
    value.bind(function (newval) {
      $('#content-wrapper a').css('color', newval)
    })
  })

  // Content font Color Hover
  wp.customize('font_color_hover_content_setting', function (value) {
    value.bind(function (newval) {

      $('#content-wrapper a').hover(function () {

          $(this).css('color', newval)
        },
        function () {           var color = wp.customize(
          'font_color_link_content_setting').get()
          $(this).css('color', color)}
      )
    })
  })


  // Content Paging font Color
  wp.customize('font_color_content_paging_setting', function (value) {
    value.bind(function (newval) {
      $('#content-wrapper .nav-links span').css('color', newval)
      $('#content-wrapper .nav-links a').css('color', newval)
    })
  })

  // Content Paging font Color Hover
  wp.customize('font_color_hover_content_paging_setting', function (value) {
    value.bind(function (newval) {

      $('#content-wrapper .nav-links a').hover(function () {

          $(this).css('color', newval)
        },
        function () {           var color = wp.customize(
          'font_color_content_paging_setting').get()
          $(this).css('color', color)}
      )
    })
  })


  // Full Header Background Color
  wp.customize('background_color_full_header_setting', function (value) {
    value.bind(function (newval) {

      var rgba = hexToRgbA(newval, 1)

      $('#full-header-wrapper').css('background-color', rgba)
    })
  })

  // Full Header Background Cover
  wp.customize('background_image_cover_full_header_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#full-header-wrapper').css('background-size', 'cover')
      }
      else {
        $('#full-header-wrapper').css('background-size', 'auto')
      }

    })
  })

  // Header Background Color
  wp.customize('background_color_header_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('background_color_opacity_header_setting').
        get()
      var rgba = hexToRgbA(newval, opacity)

      $('.header-wrapper').css('background-color', rgba)
    })
  })

  // Header Background Color Opacity
  wp.customize('background_color_opacity_header_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('background_color_header_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('.header-wrapper').css('background-color', rgba)
    })
  })

  // Header Background Cover
  wp.customize('background_image_cover_header_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('.header-wrapper').css('background-size', 'cover')
      }
      else {
        $('.header-wrapper').css('background-size', 'auto')
      }

    })
  })

  // Header Border Color
  wp.customize('border_color_header_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('border_color_opacity_header_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('.header-wrapper').css('border-bottom', '1px solid ' + rgba)

    })
  })

  // Header Border Opacity
  wp.customize('border_color_opacity_header_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('border_color_header_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('.header-wrapper').css('border-bottom', '1px solid ' + rgba)

    })
  })

  // Meta Main Background Color
  wp.customize('background_color_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('background_color_opacity_meta_main_setting').
        get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-main-wrapper').css('background-color', rgba)
    })
  })

  // Meta Main Background Color Opacity
  wp.customize('background_color_opacity_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('background_color_meta_main_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#meta-main-wrapper').css('background-color', rgba)
    })
  })

  // Meta Main Background Cover
  wp.customize('background_image_cover_meta_main_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#meta-main-wrapper').css('background-size', 'cover')
      }
      else {
        $('#meta-main-wrapper').css('background-size', 'auto')
      }

    })
  })

  // Meta Main Border Color
  wp.customize('border_color_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('border_color_opacity_meta_main_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-main-wrapper').css('border-bottom', '1px solid ' + rgba)

    })
  })

  // Meta Main Border Opacity
  wp.customize('border_color_opacity_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('border_color_meta_main_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#meta-main-wrapper').css('border-bottom', '1px solid ' + rgba)
    })
  })

  // Meta Main Submenu Background Color
  wp.customize('submenu_background_color_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize(
        'submenu_background_color_opacity_meta_main_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-main-wrapper .dropdown-menu').css('background-color', rgba)
    })
  })

  // Meta Main Submenu Background Color Opacity
  wp.customize('submenu_background_color_opacity_meta_main_setting',
    function (value) {
      value.bind(function (newval) {

        var color = wp.customize('submenu_background_color_meta_main_setting').
          get()
        var rgba = hexToRgbA(color, newval)

        $('#meta-main-wrapper .dropdown-menu').css('background-color', rgba)
      })
    })

  // Meta Main Submenu Background Image Cover
  wp.customize('submenu_background_image_cover_meta_main_setting',
    function (value) {
      value.bind(function (newval) {

        if (newval == 1) {
          $('#meta-main-wrapper .dropdown-menu').
            css('background-size', 'cover')
        }
        else {
          $('#meta-main-wrapper .dropdown-menu').css('background-size', 'auto')
        }

      })
    })

  // Meta Main Submenu Border Color
  wp.customize('submenu_border_color_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize(
        'submenu_border_color_opacity_meta_main_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-main-wrapper .dropdown-menu').css('border', '1px solid ' + rgba)
    })
  })

  // Meta Main Submenu Border Color Opacity
  wp.customize('submenu_border_color_opacity_meta_main_setting',
    function (value) {
      value.bind(function (newval) {

        var color = wp.customize('submenu_border_color_meta_main_setting').get()
        var rgba = hexToRgbA(color, newval)

        $('#meta-main-wrapper .dropdown-menu').
          css('border', '1px solid ' + rgba)
      })
    })

  // Meta Main Submenu Background Hover Color
  wp.customize('submenu_background_hover_color_meta_main_setting',
    function (value) {
      value.bind(function (newval) {

        $('#meta-main-wrapper .dropdown-menu a').hover(function () {

            var opacity = wp.customize(
              'submenu_background_hover_color_opacity_meta_main_setting').get()
            var rgba = hexToRgbA(newval, opacity)

            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )

      })
    })

  // Meta Main Submenu Background Hover Color Opacity
  wp.customize('submenu_background_hover_color_opacity_meta_main_setting',
    function (value) {
      value.bind(function (newval) {

        $('#meta-main-wrapper .dropdown-menu a').hover(function () {
            var color = wp.customize(
              'submenu_background_hover_color_meta_main_setting').get()
            var rgba = hexToRgbA(color, newval)
            $(this).css('background-color', rgba)
          },
          function () { $(this).css('background-color', 'transparent')}
        )
      })
    })

  // Meta Main Divider Color
  wp.customize('divider_color_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('divider_color_opacity_meta_main_setting').
        get()
      var rgba = hexToRgbA(newval, opacity)

      $('#meta-main-wrapper nav').css('border-left', '1px solid ' + rgba)
      $('#meta-main-wrapper nav .nav-item').
        css('border-right', '1px solid ' + rgba)

    })
  })

  // Meta Main Divider Opacity
  wp.customize('divider_color_opacity_meta_main_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('divider_color_meta_main_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#meta-main-wrapper nav').css('border-left', '1px solid ' + rgba)
      $('#meta-main-wrapper nav .nav-item').
        css('border-right', '1px solid ' + rgba)
    })
  })

  // Footer Background Color
  wp.customize('background_color_footer_setting', function (value) {
    value.bind(function (newval) {

      var rgba = hexToRgbA(newval, 1)

      $('body').css('background-color', rgba)
    })
  })

  // Footer Background Cover
  wp.customize('background_image_cover_footer_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('body').css('background-size', 'cover')
      }
      else {
        $('body').css('background-size', 'auto')
      }

    })
  })

  // Footer Border Color
  wp.customize('border_color_footer_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('border_color_opacity_footer_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#footer-wrapper .widget ul li').css('border-top', '1px solid ' + rgba)
    })
  })

  // Footer Border Opacity
  wp.customize('border_color_opacity_footer_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('border_color_footer_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#footer-wrapper .widget ul li').css('border-top', '1px solid ' + rgba)
    })
  })

  // Content Background Color
  wp.customize('background_color_content_setting', function (value) {
    value.bind(function (newval) {

      var rgba = hexToRgbA(newval, 1)

      $('#content-wrapper').css('background-color', rgba)
    })
  })

  // Content Background Cover
  wp.customize('background_image_cover_content_setting', function (value) {
    value.bind(function (newval) {

      if (newval == 1) {
        $('#content-wrapper').css('background-size', 'cover')
      }
      else {
        $('#content-wrapper').css('background-size', 'auto')
      }

    })
  })

  // Content Border Color
  wp.customize('border_color_content_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('border_color_opacity_content_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#content-wrapper .widget ul li').css('border-top', '1px solid ' + rgba)
      $('#content-wrapper').css('border-bottom', '1px solid ' + rgba)
    })
  })

  // Content Border Opacity
  wp.customize('border_color_opacity_content_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('border_color_content_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#content-wrapper .widget ul li').css('border-top', '1px solid ' + rgba)
      $('#content-wrapper').css('border-bottom', '1px solid ' + rgba)
    })
  })


  // Content Paging Background Color
  wp.customize('background_color_content_paging_setting', function (value) {
    value.bind(function (newval) {

      var opacity = wp.customize('background_color_content_paging_opacity_setting').get()
      var rgba = hexToRgbA(newval, opacity)

      $('#content-wrapper .page-numbers.current').css('background-color', rgba)

      $('#content-wrapper a.page-numbers').hover(function () {
          var opacity = wp.customize('background_color_content_paging_opacity_setting').get()
          var rgba = hexToRgbA(newval, opacity)

          $(this).css('background-color', rgba)
        },
        function () { $(this).not('.current').css('background-color', 'transparent')}
      )

    })
  })

  // Content Paging Background Opacity
  wp.customize('background_color_content_paging_opacity_setting', function (value) {
    value.bind(function (newval) {

      var color = wp.customize('background_color_content_paging_setting').get()
      var rgba = hexToRgbA(color, newval)

      $('#content-wrapper .page-numbers.current').css('background-color', rgba)

      $('#content-wrapper a.page-numbers').hover(function () {
          var color = wp.customize('background_color_content_paging_setting').get()
          var rgba = hexToRgbA(color, newval)

          $(this).css('background-color', rgba)
        },
        function () { $(this).not('.current').css('background-color', 'transparent')}
      )

    })
  })

})(jQuery)