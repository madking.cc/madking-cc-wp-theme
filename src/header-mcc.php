<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="full-header-wrapper">

<?php
get_template_part( 'templates/meta/menu', 'top' );
?>
	<div id="header-wrapper" class="header-wrapper">
		<div class="container">
            <div class="row">
			<?php if(has_header_image())
			{
				?><div id="logo-big" class="col-1 logo-header">
                <a class='hvr-buzz' href="<?php echo get_option( 'home' ); ?>"><?php echo get_header_image_tag(); ?></a>
                </div>
				<?php
			}
			?>
			<?php if ( display_header_text() )  { ?>
                <div class="col">
                <h1 id="title"><a class='hvr-pulse' href="<?php echo get_option( 'home' ); ?>"><?php bloginfo( "name" ); ?></a></h1>
                <div id="slogan">
                    <p id="description"><?php bloginfo( "description" ); ?></p>
                </div>
                </div>
			<?php } ?>
            </div>
		</div>
	</div>
<?php

get_template_part( 'templates/meta/menu', 'main' );

?>
</div>
<div id="content-wrapper">
	<div class="container">
		<?php get_template_part('templates/nav/nav', 'posts'); ?>
		<div class="row">
			<div class="col" id="content">
