<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
get_header("mcc");
?>

	<div class="wrap">

		<header class="page-header">
			<?php if ( have_posts() ) : $mcc_have_posts = true; ?>
				<h1 class="page-title"><?php printf( __( 'Search Results for: %s', MCC_THEME_TXT ), '<span>' . get_search_query() . '</span>' ); ?></h1>
			<?php else : ?>
				<h1 class="page-title"><?php _e( 'Nothing Found', MCC_THEME_TXT ); ?></h1>
			<?php endif; ?>
		</header><!-- .page-header -->

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php
				if ( have_posts() ) : $mcc_have_posts = true;
					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/**
						 * Run the loop for the search to output the results.
						 * If you want to overload this in a child theme then include a file
						 * called content-search.php and that will be used instead.
						 */
						get_template_part( 'templates/post/content' );

					endwhile; // End of the loop.

				else : ?>

					<p><?php _e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', MCC_THEME_TXT ); ?></p>
					<div class="search-form-wrapper">
					<?php
					get_search_form();
					?></div><?php
				endif;
				?>

			</main><!-- #main -->
		</div><!-- #primary -->

	</div><!-- .wrap -->

<?php

get_footer("mcc");
get_footer();