<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( post_password_required() ) {
	return;
}
?>

<div id="comments" class="comments-area">

	<?php
	if ( have_comments() ):
		//We have comments
		?>

        <p class="comment-title"><strong>
				<?php

				printf( esc_html__( _nx( 'One comment on &ldquo;%2$s&rdquo;', '%1$s comments on &ldquo;%2$s&rdquo;', get_comments_number(), 'comments title', MCC_THEME_TXT ) ), number_format_i18n( get_comments_number() ), '<span>' . get_the_title() . '</span>' );

				?>
            </strong></p>

		<?php $GLOBALS['mcc_theme']->get_comments_navigation(); ?>

        <ol class="comment-list">

			<?php

			$args = [
				'walker'            => null,
				'max_depth'         => '',
				'style'             => 'ol',
				'callback'          => null,
				'end-callback'      => null,
				'type'              => 'all',
				'reply_text'        => __( 'Reply', MCC_THEME_TXT ),
				'page'              => '',
				'per_page'          => '',
				'avatar_size'       => 64,
				'reverse_top_level' => null,
				'reverse_children'  => '',
				'format'            => 'html5',
				'short_ping'        => false,
				'echo'              => true,
			];

			wp_list_comments( $args );
			?>

        </ol>

		<?php $GLOBALS['mcc_theme']->get_comments_navigation(); ?>

		<?php
		if ( ! comments_open() && get_comments_number() ):
			?>

            <p class="no-comments"><?php esc_html_e( 'Comments are closed.', MCC_THEME_TXT ); ?></p>

			<?php
		endif;
		?>

		<?php
	endif;
	?>

	<?php

	$fields = [

		'author' => '<div class="form-group"><label for="author">' . __( 'Name', MCC_THEME_TXT ) . '</label> <span class="required">*</span> <input id="author" name="author" type="text" class="form-control" value="' . esc_attr( $commenter['comment_author'] ) . '" required="required" /></div>',

		'email' => '<div class="form-group"><label for="email">' . __( 'Email', MCC_THEME_TXT ) . '</label> <span class="required">*</span><input id="email" name="email" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author_email'] ) . '" required="required" /></div>',

		'url' => '<div class="form-group last-field"><label for="url">' . __( 'Website', MCC_THEME_TXT ) . '</label><input id="url" name="url" class="form-control" type="text" value="' . esc_attr( $commenter['comment_author_url'] ) . '" /></div>',

	];

	$args = [

		'class_submit'  => 'btn btn-block btn-lg btn-primary',
		'label_submit'  => __( 'Submit Comment', MCC_THEME_TXT ),
		'comment_field' => '<div class="form-group"><label for="comment">' . _x( 'Comment', 'noun', MCC_THEME_TXT ) . '</label> <span class="required">*</span><textarea id="comment" class="form-control" name="comment" rows="4" required="required"></textarea></div>',
		'fields'        => apply_filters( 'comment_form_default_fields', $fields ),

	];

	comment_form( $args );

	?>

</div><!-- .comments-area -->