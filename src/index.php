<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();

		if ( is_home() && ! is_front_page() ) :

			get_header("mcc-frontpage");
			?>
			<header class="page-header">
				<h1 class="page-title"><?php single_post_title(); ?></h1>
			</header>
		<?php else :
			get_header("mcc");
			?>
			<header class="page-header">
				<h2 class="page-title"><?php _e( 'Posts', MCC_THEME_TXT ); ?></h2>
			</header>
		<?php endif; ?>

		<div id="primary" class="content-area">
			<main id="main" class="site-main" role="main">

				<?php

				if ( have_posts() ) :
					$mcc_have_posts = true;

					/* Start the Loop */
					while ( have_posts() ) : the_post();

						/*
						 * Include the Post-Format-specific template for the content.
						 * If you want to override this in a child theme, then include a file
						 * called content-___.php (where ___ is the Post Format name) and that will be used instead.
						 */
						get_template_part( 'templates/post/content', get_post_format() );


					endwhile;

				else :

					get_template_part( 'templates/post/content', 'none' );

				endif;
				?>

			</main><!-- #main -->
		</div><!-- #primary -->

<?php
get_footer("mcc");
get_footer();