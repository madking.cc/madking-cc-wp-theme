<?php
/**
 * Displays content for front page
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class( 'mcc-panel ' ); ?> >

	<?php if ( has_post_thumbnail() ) :
		$post_thumnail_url = mcc_functions::post_thumbnail_url($post);
		?>
    <div class="row">
        <div class="move-to-border-left article-preview-thumbnail">
        <div class="panel-image post-thumbnail"
             style="height: 100%; background-image: url('<?php echo $post_thumnail_url; ?>');background-size:cover;background-position: center;">
		    <?php if ( ! is_single() ) { ?>

                <a href="<?php the_permalink($post); ?>" style="display:block; width: 100%; height: 100%;">
                </a>
		    <?php } ?>
        </div><!-- .post-thumbnail -->
    </div>
       <?php if ('' !== get_the_post_thumbnail()) {
        ?>
        <div class="entry-col">
            <?php } else { ?>
            <div class="">
                <?php   } ?>
	<?php endif; ?>

	<div class="panel-content">
		<div class="wrap">
			<header class="entry-header">
				<?php the_title( '<h2 class="entry-title hvr-push"><a href="'.get_the_permalink($post).'">', '</a></h2>' ); ?>
                <?php mcc_functions::edit_link( get_the_ID() ); ?>
			</header><!-- .entry-header -->

			<div class="entry-content">
				<?php
					/* translators: %s: Name of current post */
					the_content( sprintf(
						__( 'Continue reading<span class="screen-reader-text"> "%s"</span>', MCC_THEME_TXT ),
						get_the_title()
					) );
				?>
			</div><!-- .entry-content -->
            <?php
            mcc_functions::entry_footer();
            ?>
		</div><!-- .wrap -->
    </div>
<?php if ( has_post_thumbnail() ) : ?>
            </div>
        </div>
        <?php endif; ?>

</article><!-- #post-## -->
