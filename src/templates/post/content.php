<?php
/**
 * Template part for displaying posts
 * @link       https://codex.wordpress.org/Template_Hierarchy
 * @package    WordPress
 * @subpackage Twenty_Seventeen
 * @since      1.0
 * @version    1.2
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <?php

    $previous_post = get_previous_post();

    $next_post = get_next_post();


    if (is_sticky() && is_home()) :
        //echo mcc_get_svg( array( 'icon' => 'thumb-tack' ) );
    endif;
    ?>
    <?php if (!is_single()) { ?>

    <div class="row">

        <?php } else {

            if ($previous_post) {
                echo '<div class="row article-link-previous"><div class="col"><a class="left" href="' . esc_url(get_permalink($previous_post->ID)) . '#jtitle"><i class="fa fa-angle-double-left"></i> ' . esc_html($previous_post->post_title) . '</a></div></div>';
            }
        }
        ?>

        <?php if ('' !== get_the_post_thumbnail()) :
        $post_thumnail_url = mcc_functions::post_thumbnail_url();
        if (!is_single()) {
        ?>
        <div class="move-to-border-left article-preview-thumbnail">
            <?php } else {

            ?>
            <div class="move-to-border-top article-thumbnail-header" style="height: 188px;">
                <?php } ?>
                <div class="post-thumbnail"
                     style="height: 100%; background-image: url('<?php echo $post_thumnail_url; ?>');height: 0;padding: 0;padding-bottom: 75%;background-position: center center;background-size: 100%;background-repeat: no-repeat;">
                    <?php if (!is_single()) { ?>

                        <a href="<?php the_permalink(); ?>" style="display:block; width: 100%; height: 100%;">
                        </a>
                    <?php } ?>
                </div><!-- .post-thumbnail -->
            </div>
            <?php endif;

            if (!is_single()) {

            if ('' !== get_the_post_thumbnail()) {
                ?>
            <div class="entry-col">
                <?php } else { ?>
                <div class="">
                 <?php   } ?>

                <?php } else { ?>
                <div>
                    <?php } ?>
                    <header class="entry-header">
                        <?php

                        if ('post' === get_post_type()) {
                            echo '<div class="entry-meta">';
                            if (is_single()) {
                                mcc_functions::posted_on();
                            } else {
                                echo mcc_functions::time();
                                mcc_functions::edit_link();
                            };
                            echo '</div><!-- .entry-meta -->';
                        };

                        if (is_single()) {
                            the_title('<h1 class="entry-title">', '</h1>');
                        } elseif (is_front_page() && is_home()) {
                            the_title('<h3 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h3>');
                        } else {
                            the_title('<h2 class="entry-title"><a href="' . esc_url(get_permalink()) . '" rel="bookmark">', '</a></h2>');
                        }
                        ?>
                    </header><!-- .entry-header -->
                    <div class="entry-content">
                        <?php
                        if (true || is_single()) {
                            /* translators: %s: Name of current post */
                            the_content(sprintf(__('Continue reading<span class="screen-reader-text"> "%s"</span>', MCC_THEME_TXT), get_the_title()));
                        } else {
                            the_excerpt();
                        }
                        wp_link_pages([
                            'before' => '<div class="page-links">' . __('Pages:', MCC_THEME_TXT),
                            'after' => '</div>',
                            'link_before' => '<span class="page-number">',
                            'link_after' => '</span>',
                        ]);
                        ?>
                    </div><!-- .entry-content -->
                </div><!-- .col -->

                <?php
                if (is_single()) {
                    mcc_functions::entry_footer();

                    if ($next_post) {
                        echo '<div class="row article-link-next"><div class="col"><a class="left" href="' . esc_url(get_permalink($next_post->ID)) . '#jtitle"><i class="fa fa-angle-double-right"></i> ' . esc_html($next_post->post_title) . '</a></div></div>';
                    }

                }
                ?>
                <?php if (!is_single()) { ?>
            </div><!-- .row -->
        <?php } ?>
</article><!-- #post-## -->
