<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<nav class="comment-navigation" role="navigation">
    <div class="row">
        <div class="col-xs-12 col-sm-6">
            <div class="post-link-nav">
                <span class="mcc-icon mcc-chevron-left" aria-hidden="true"></span>
				<?php previous_comments_link( esc_html__( 'Older Comments', MCC_THEME_TXT ) ) ?>
            </div>
        </div>
        <div class="col-xs-12 col-sm-6 text-right">
            <div class="post-link-nav">
				<?php next_comments_link( esc_html__( 'Newer Comments', MCC_THEME_TXT ) ) ?>
                <span class="mcc-icon mcc-chevron-right" aria-hidden="true"></span>
            </div>
        </div>
    </div><!-- .row -->
</nav>