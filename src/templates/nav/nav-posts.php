<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>


    <div class="row nav-wrapper">
        <div class="col">

			<?php

			the_posts_pagination( array(
				'prev_text' => '<< <span class="screen-reader-text">' . __( 'Back', MCC_THEME_TXT ) . '</span>',
				'next_text' => '<span class="screen-reader-text">' . __( 'Next', MCC_THEME_TXT ) . '</span> >>',
				'before_page_number' => '<span class="meta-nav screen-reader-text">' . __( 'Page', MCC_THEME_TXT ) . ' </span>',
			) );
			?>

        </div><!-- .col-xx-xx -->
    </div><!-- .row -->

