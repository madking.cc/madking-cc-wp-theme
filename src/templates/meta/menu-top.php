<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}
?>
<div id="meta-top-wrapper">
    <div class="container">

        <div id="meta-top-search-wrapper-collapsed">
            <ul class="navbar-nav mr-auto">
                <li>
                    <?php get_search_form(); ?>
                </li>
            </ul>
        </div>

        <div id="meta-top-nav-wrapper">

            <header class="clearfix navbar-light ">


                <button type="button"
                        class="navbar-toggler navbar-toggle offcanvas-toggle bg-light"
                        data-toggle="offcanvas" data-target="#meta-top-nav"
                        aria-controls="meta-top-nav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="navbar-toggler-icon"></span>
                </button>

                <nav class="navbar  navbar-offcanvas navbar-offcanvas-touch navbar-expand-sm" id="meta-top-nav">

                <div class="container-fluid" >
					<?php
					wp_nav_menu( [
						"theme_location" => "meta-top-nav",
						'menu'           => "meta-top-nav",
						'depth'          => 2,
						"container"      => false,
						"menu_class"     => "navbar-nav mr-auto",
						"fallback_cb"    => "mcc_nav_walker::fallback",
						"walker"         => new mcc_nav_walker(),
					] );
					?>
                    <div id="meta-top-search-wrapper">
                        <ul class="navbar-nav mr-auto">
                            <li>
								<?php get_search_form(); ?>
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            </header>
        </div>
    </div>
</div>