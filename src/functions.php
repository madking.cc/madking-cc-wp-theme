<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/*
* Absoluter Pfad zum Theme
* string
*/
if ( ! defined( 'MCC_THEME_ROOT' ) ) {
	define( 'MCC_THEME_ROOT', dirname( __FILE__ ) );
}

/*
* URL Adresse zum Theme
* string
*/
if ( ! defined( 'MCC_THEME_URL' ) ) {
	define( 'MCC_THEME_URL', get_template_directory_uri() );
}


/*
* Textdomain für Übersetzungen
* string
*/
if ( ! defined( 'MCC_THEME_TXT' ) ) {
	define( 'MCC_THEME_TXT', "mcc" );
}

// Post Typen und Taxonomies erstellen
require_once( MCC_THEME_ROOT . "/classes/post_types/imagelink.php" );

//Füge Klassen hinzu:
require_once( MCC_THEME_ROOT . "/classes/mcc_theme.php" );
require_once( MCC_THEME_ROOT . "/classes/mcc_nav_walker.php" );

/*
* Starte die Hauptklasse
*/
$mcc_theme           = mcc_theme::instance();

/*
* Auf true, wenn Posts vorhanden
*/
$mcc_have_posts = false;

/**
 * Bindet Header Angaben ein
 * @since  1.0.0
 * @return void
 */
function get_mcc_header($type = "sub")
{
	switch($type) {
		case "front":
			get_template_part( 'templates/header', 'frontpage' );
			break;
		default:
			get_template_part( 'templates/header', 'subpages' );
			break;
	}
}

/**
 * Bindet Footer Angaben ein
 * @since  1.0.0
 * @return void
 */
function get_mcc_footer()
{

	get_template_part( 'templates/footer', 'all' );

}