<?php
/**
 * The sidebar containing the main widget area
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package WordPress
 * @subpackage Twenty_Seventeen
 * @since 1.0
 * @version 1.0
 */

$sidebar_col_suffix = "";
if(isset($GLOBALS["mcc_have_posts"]) && $GLOBALS["mcc_have_posts"])
	$sidebar_col_suffix = "-3";

if ( is_home() && ! is_front_page() ) {
	if ( is_active_sidebar( 'mcc-frontpage-main-content-1' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-frontpage-main-content-1' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='frontpage-sidebar-1'>";
			echo $sidebar;
			echo "</div>";
		}
	}
	if ( is_active_sidebar( 'mcc-frontpage-main-content-2' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-frontpage-main-content-2' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='frontpage-sidebar-2'>";
			echo $sidebar;
			echo "</div>";
		}
	}
	if ( is_active_sidebar( 'mcc-frontpage-main-content-3' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-frontpage-main-content-3' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='frontpage-sidebar-3'>";
			echo $sidebar;
			echo "</div>";
		}
	}
}
else
{
	if ( is_active_sidebar( 'mcc-main-content-1' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-main-content-1' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='page-sidebar-1'>";
			echo $sidebar;
			echo "</div>";
		}
	}
	if ( is_active_sidebar( 'mcc-main-content-2' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-main-content-2' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='page-sidebar-2'>";
			echo $sidebar;
			echo "</div>";
		}
	}
	if ( is_active_sidebar( 'mcc-main-content-3' ) ) {
		ob_start();
		dynamic_sidebar( 'mcc-main-content-3' );
		$sidebar = ob_get_clean();
		if ($sidebar) {
			echo "<div class='col".$sidebar_col_suffix."' id='page-sidebar-3'>";
			echo $sidebar;
			echo "</div>";
		}
	}
}