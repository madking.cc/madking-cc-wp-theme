<?php

// Danke an https://github.com/RRZE-Webteam/FAU-Einrichtungen/blob/master/functions/posttype_imagelink.php

/*
 * Verwaltung der Bildlinks / Logos
 */

function mcc_san($s){
	return filter_var(trim($s), FILTER_SANITIZE_STRING);
}

function mcc_form_url($name= '', $prevalue = '', $labeltext = '', $howtotext = '', $placeholder='http://', $size = 0) {
	$name = mcc_san( $name );
	$labeltext = mcc_san( $labeltext );
	if (isset($name) &&  isset($labeltext))  {
		echo "<p>\n";
		echo '	<label for="'.$name.'">';
		echo $labeltext;
		echo "</label><br />\n";
		echo '	<input type="url" class="large-text" name="'.$name.'" id="'.$name.'" value="'.$prevalue.'"';
		if (strlen(trim($placeholder))) {
			echo ' placeholder="'.$placeholder.'"';
		}
		if (intval($size)>0) {
			echo ' length="'.$size.'"';
		}
		echo " />\n";
		echo "</p>\n";
		if (strlen(trim($howtotext))) {
			echo '<p class="howto">';
			echo $howtotext;
			echo "</p>\n";
		}
	} else {
		echo _('Ungültiger Aufruf von fau_form_url() - Name oder Label fehlt.', MCC_THEME_TXT);
	}
}

function mcc_form_text($name= '', $prevalue = '', $labeltext = '', $howtotext = '', $placeholder='', $size = 0) {
	$name = mcc_san( $name );
	$labeltext = mcc_san( $labeltext );
	if (isset($name) &&  isset($labeltext))  {
		echo "<p>\n";
		echo '	<label for="'.$name.'">';
		echo $labeltext;
		echo "</label><br />\n";

		echo '	<input type="text" ';
		if (intval($size)>0) {
			echo ' size="'.$size.'"';
		} else {
			echo ' class="large-text"';
		}
		echo ' name="'.$name.'" id="'.$name.'" value="'.$prevalue.'"';
		if (strlen(trim($placeholder))) {
			echo ' placeholder="'.$placeholder.'"';
		}

		echo " />\n";
		echo "</p>\n";
		if (strlen(trim($howtotext))) {
			echo '<p class="howto">';
			echo $howtotext;
			echo "</p>\n";
		}
	} else {
		echo _('Ungültiger Aufruf von fau_form_text() - Name oder Label fehlt.', MCC_THEME_TXT);
	}
}

function mcc_imagelink_taxonomy() {
	register_taxonomy(
		'mcc_imagelinks_category',  //The name of the taxonomy. Name should be in slug form (must not contain capital letters or spaces).
		'mcc_imagelink',   		 //post type name
		array(
			'hierarchical' 		=> true,
			'label' 			=> __('Bildlink-Kategorien', MCC_THEME_TXT),  //Display name
			'query_var' 		=> true,
			'rewrite'			=> array(
				'slug' 			=> 'mcc_imagelinks', // This controls the base slug that will display before each term
				'with_front' 	=> false // Don't display the category base before
			)
		)
	);
}
add_action( 'init', 'mcc_imagelink_taxonomy');

// Register Custom Post Type
function mcc_imagelink_post_type() {

	$labels = array(
		'name'                => _x( 'Bildlinks', 'Post Type General Name', MCC_THEME_TXT ),
		'singular_name'       => _x( 'Bildlink', 'Post Type Singular Name', MCC_THEME_TXT ),
		'menu_name'           => __( 'Bildlinks', MCC_THEME_TXT ),
		'parent_item_colon'   => __( 'Übergeordneter Bildlink', MCC_THEME_TXT ),
		'all_items'           => __( 'Alle Bildlinks', MCC_THEME_TXT ),
		'view_item'           => __( 'Bildlink anzeigen', MCC_THEME_TXT ),
		'add_new_item'        => __( 'Neuen Bildlink einfügen', MCC_THEME_TXT ),
		'add_new'             => __( 'Neuer Bildlink', MCC_THEME_TXT ),
		'edit_item'           => __( 'Bildlink bearbeiten', MCC_THEME_TXT ),
		'update_item'         => __( 'Bildlink aktualisieren', MCC_THEME_TXT ),
		'search_items'        => __( 'Bildlink suchen', MCC_THEME_TXT ),
		'not_found'           => __( 'Keine Bildlinks gefunden', MCC_THEME_TXT ),
		'not_found_in_trash'  => __( 'Keine Bildlinks im Papierkorb gefunden', MCC_THEME_TXT ),
	);
	$args = array(
		'label'               => __( 'mcc_imagelink', MCC_THEME_TXT ),
		'description'         => __( 'Bildlink-Eigenschaften', MCC_THEME_TXT ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'thumbnail' ),
		'taxonomies'          => array( 'mcc_imagelinks_category' ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_icon'		=> 'dashicons-format-image',
		'can_export'          => true,
		'has_archive'         => false,
		'exclude_from_search' => true,
		'publicly_queryable'  => true,
		'query_var'           => 'mcc_imagelink',
		'rewrite'             => false,
		/*	'capability_type'     => 'mcc_imagelink',
			'capabilities' => array(
				'edit_post' => 'edit_imagelink',
				'read_post' => 'read_imagelink',
				'delete_post' => 'delete_imagelink',
				'edit_posts' => 'edit_imagelinks',
				'edit_others_posts' => 'edit_others_imagelinks',
				'publish_posts' => 'publish_imagelinks',
				'read_private_posts' => 'read_private_imagelinks',
				'delete_posts' => 'delete_imagelinks',
				'delete_private_posts' => 'delete_private_imagelinks',
				'delete_published_posts' => 'delete_published_imagelinks',
				'delete_others_posts' => 'delete_others_imagelinks',
				'edit_private_posts' => 'edit_private_imagelinks',
				'edit_published_posts' => 'edit_published_imagelinks'
			),
			'map_meta_cap' => true

		 */
	);
	register_post_type( 'mcc_imagelink', $args );

}

// Hook into the 'init' action
add_action( 'init', 'mcc_imagelink_post_type', 0 );


function mcc_imagelink_restrict_manage_posts() {
	global $typenow;

	if( $typenow == "mcc_imagelink" ){
		$filters = get_object_taxonomies($typenow);

		foreach ($filters as $tax_slug) {
			$tax_obj = get_taxonomy($tax_slug);
			wp_dropdown_categories(array(
				'show_option_all' => sprintf(__('Alle %s anzeigen', MCC_THEME_TXT), $tax_obj->label),
				'taxonomy' => $tax_slug,
				'name' => $tax_obj->name,
				'orderby' => 'name',
				'selected' => isset($_GET[$tax_slug]) ? $_GET[$tax_slug] : '',
				'hierarchical' => $tax_obj->hierarchical,
				'show_count' => true,
				'hide_if_empty' => true
			));
		}

	}
}
add_action( 'restrict_manage_posts', 'mcc_imagelink_restrict_manage_posts' );



function mcc_imagelink_post_types_admin_order( $wp_query ) {
	if ((is_admin()) && ($wp_query)) {
		if (isset($wp_query->query['post_type'])) {
			$post_type = $wp_query->query['post_type'];
			if ( $post_type == 'mcc_imagelink') {

				if( ! isset($wp_query->query['orderby']))
				{
					$wp_query->set('orderby', 'title');
					$wp_query->set('order', 'ASC');
				}

			}
		}
	}
}
add_filter('pre_get_posts', 'mcc_imagelink_post_types_admin_order');



function mcc_imagelink_metabox() {
	add_meta_box(
		'mcc_imagelink_metabox',
		__( 'Eigenschaften', MCC_THEME_TXT ),
		'mcc_imagelink_metabox_content',
		'mcc_imagelink',
		'normal',
		'high'
	);
}
function mcc_imagelink_metabox_content( $object, $box ) {
	global $defaultoptions;
	global $post;


	wp_nonce_field( basename( __FILE__ ), 'mcc_imagelink_metabox_content_nonce' );

	if ( !current_user_can( 'edit_page', $object->ID) )
		// Oder sollten wir nach publish_pages  fragen?
		// oder nach der Rolle? vgl. http://docs.appthemes.com/tutorials/wordpress-check-user-role-function/
		return;


	$targeturl = get_post_meta( $object->ID, 'srval_imagelink_url', true );

	/* Old values */
	$desc  = get_post_meta( $object->ID, 'portal_description', true );
	$protocol  = get_post_meta( $object->ID, 'protocol', true );
	$link  = get_post_meta( $object->ID, 'link', true );

	if (empty($targeturl) && isset($protocol) && isset($link)) {
		$targeturl = $protocol.$link;
	}

	mcc_form_url('mcc_imagelink_url', $targeturl, __('Webadresse',MCC_THEME_TXT), '', $placeholder='http://');
	mcc_form_text('mcc_imagelink_desc', $desc, __('Kurzbeschreibung',MCC_THEME_TXT));

	return;

}


add_action( 'add_meta_boxes', 'mcc_imagelink_metabox' );






function mcc_imagelink_metabox_content_save( $post_id ) {
	global $options;
	if (  'mcc_imagelink'!= get_post_type()  ) {
		return;
	}


	if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE )
		return;


	if ( !isset( $_POST['mcc_imagelink_metabox_content_nonce'] ) || !wp_verify_nonce( $_POST['mcc_imagelink_metabox_content_nonce'], basename( __FILE__ ) ) )
		return $post_id;



	if ( 'page' == $_POST['post_type'] ) {
		if ( !current_user_can( 'edit_page', $post_id ) )
			return;
	} else {
		if ( !current_user_can( 'edit_post', $post_id ) )
			return;
	}

	/* Old values */
	$targeturl = get_post_meta( $post_id, 'srval_imagelink_url', true );
	$protocol  = get_post_meta( $post_id, 'protocol', true );
	$link  = get_post_meta( $post_id, 'link', true );


	$newval = ( isset( $_POST['mcc_imagelink_desc'] ) ? sanitize_text_field( $_POST['mcc_imagelink_desc'] ) : 0 );
	$oldval =  get_post_meta( $post_id, 'portal_description', true );

	if (!empty(trim($newval))) {
		if (isset($oldval)  && ($oldval != $newval)) {
			update_post_meta( $post_id, 'portal_description', $newval );
		} else {
			add_post_meta( $post_id, 'portal_description', $newval, true );
		}
	} elseif ($oldval) {
		delete_post_meta( $post_id, 'portal_description', $oldval );
	}


	if (empty($targeturl) && isset($protocol) && isset($link)) {
		$targeturl2 = $protocol.$link;
	}

	if (filter_var($_POST['mcc_imagelink_url'], FILTER_VALIDATE_URL)) {
		$newval = $_POST['mcc_imagelink_url'];
	}
	if (!empty($newval)) {
		if (isset($targeturl)  && ($targeturl != $newval)) {
			update_post_meta( $post_id, 'srval_imagelink_url', $newval );
		} else {
			add_post_meta( $post_id, 'srval_imagelink_url', $newval, true );
		}
	} else {
		if ($targeturl) {
			delete_post_meta( $post_id, 'srval_imagelink_url', $oldval );
		}
	}
	if (isset($protocol) && isset($link)) {
		delete_post_meta( $post_id, 'protocol' );
		delete_post_meta( $post_id, 'link' );
	}
}
add_action( 'save_post', 'mcc_imagelink_metabox_content_save' );



function mcc_get_imagelinks ( $catid, $echo = true ) {
	global $options;
	global $usejslibs;

	if ( isset($catid) && $catid >0) {


		$args = array(
			'post_type'	=> 'mcc_imagelink',
			'nopaging' => 1,
			'orderby' => 'name',
			'order' => 'ASC',
			'tax_query' => array(
				array(
					'taxonomy' => 'mcc_imagelinks_category',
					'field' => 'id', // can be slug or id - a CPT-onomy term's ID is the same as its post ID
					'terms' => $catid,

				)
			)
		);
		$output = '';
		$imagelist = get_posts($args);
		$item_output = '';
		$number =0;
		foreach($imagelist as $item) {
			$number++;
			$currenturl  = get_post_meta( $item->ID, 'srval_imagelink_url', true );
			if (!$currenturl) {
				$protocol  = get_post_meta( $item->ID, 'protocol', true );
				$link  = get_post_meta( $item->ID, 'link', true );
				$currenturl = $protocol.$link;
			}
			$item_output .= '<li>';
			$item_output .= '<a rel="nofollow" href="'.$currenturl.'">';


			$item_output .= get_the_post_thumbnail($item->ID, 'logo-thumb');
			/*
			 * Falls ich bei den ALT-Tag lieber auf Empty wechseln will, dann diesen Code
			 * aktivieren:
			 *
			$post_thumbnail_id = get_post_thumbnail_id( $item->ID );
			$sliderimage = wp_get_attachment_image_src( $post_thumbnail_id, 'logo-thumb' );
			$slidersrcset =  wp_get_attachment_image_srcset($post_thumbnail_id, 'logo-thumb');

			$item_output .= '<img src="'.$sliderimage[0].'" alt="" width="'.$sliderimage[1].'" height="'.$sliderimage[2].'"';
			if ($slidersrcset) {
			$item_output .= 'srcset="'.$slidersrcset.'"';
			}
			$item_output .= '>';
			*/

			$item_output .= '</a>';
			$item_output .= '</li>';



		}
		if ($number>0) {
			$output .= '<div class="imagelink_carousel">';
			$output .= '<div class="container">';
			$output .= '<div class="logos-menu-nav">';
			$output .= '<a id="logos-menu-prev" href="#"><i class="fa fa-chevron-left"></i><span class="screen-reader-text">'. __('Zurück', MCC_THEME_TXT) . '</span></a>';
			$output .= '<a id="logos-menu-next" href="#"><i class="fa fa-chevron-right"></i><span class="screen-reader-text">'. __('Weiter', MCC_THEME_TXT) . '</span></a>';
			$output .= '</div>';
			$output .= "</div>\n";
			$output .= '<ul class="logos-menu">';
			$output .= $item_output;
			$output .= "</ul>\n";

			$output .= '<div class="container">';
			$output .= '<div class="row logos-menu-settings">';
			$output .= '<a id="logos-menu-playpause" href="#">'
			           . '<span class="play"><i class="fa fa-play"></i>'. __('Abspielen', MCC_THEME_TXT) . '</span>'
			           . '<span class="pause"><i class="fa fa-pause"></i>'. __('Pause', MCC_THEME_TXT) . '</span></a>';
			$output .= '</div>';
			$output .= '</div>';
			$output .= "</div>\n";

			$usejslibs['caroufredsel'] = true;
		}
		if ($echo==true) {
			echo $output;
			return;
		} else {
			return $output;
		}




	} else {
		return;
	}
}