<?php


if ( class_exists( 'WP_Customize_Control' ) && ! class_exists( 'ThemeName_Customize_Misc_Control' ) ) :
	class Mcc_Customize_Divider_Control extends WP_Customize_Control {
		public $settings = 'mcc';
		public $description = 'Divider';

		public function render_content() {
			echo '<hr>';
			echo '<h1 class="customize-control-title">' . esc_html( $this->label ) . '</h1>';

		}
	}
endif;