<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

//Füge Klassen hinzu:
require_once( MCC_THEME_ROOT . "/classes/mcc_functions.php" );
require_once( MCC_THEME_ROOT . "/classes/widgets/mcc_widget_tax_menu.php" );
require_once( MCC_THEME_ROOT . "/classes/customizer/customizer.php" );

// Aktionen, wenn das Plugin aktiviert, bzw. deaktiviert wird:
register_activation_hook( __FILE__, [ 'mcc_theme', 'activation' ] );
register_deactivation_hook( __FILE__, [ 'mcc_theme', 'deactivation' ] );

/*
 * MCC-Theme-Haupt-Klasse
 */

class mcc_theme {

	/*
    * Name der Variable unter der die Einstellungen des Plugins gespeichert werden.
	 * @since  1.0.0
	 * @access public
	 * @var string
    */
	const option_name = 'mcc_theme';

	/*
    * Name der Variable zum Zwecke der Aktualisierung des Plugins.
	 * @since  1.0.0
	 * @access public
	 * @var string
    */
	const version_option_name = 'mcc_theme_version';

	/**
	 * Version des Plugins
	 * @since  1.0.0
	 * @access public
	 * @var string
	 */
	const version = '1.0.0';

	/*
    * Minimal erforderliche PHP-Version.
	 * @since  1.0.0
	 * @access public
	 * @var string
    */
	const php_version = '7.2';

	/*
    * Minimal erforderliche WordPress-Version.
	 * @since  1.0.0
	 * @access public
	 * @var string
    */
	const wp_version = '4.9.1';

	/*
    * Optionen des Plugins
	 * @since  1.0.0
	 * @access protected
	 * @var array
    */
	protected static $options;

	/*
    * CSS Breakpoint für Navigation
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @var string
    */
	public static $nav_toggle_break = "960px";

	/*
    * Bezieht sich auf eine einzige Instanz dieser Klasse.
	 * @since  1.0.0
	 * @access protected
	 * @var object
    */
	protected static $instance = null;

	/*
    * Bezieht sich auf die Klasse der extra Funktionen,
	 * welche oft benötigt werden
	 * @since  1.0.0
	 * @access protected
	 * @var object
    */
	protected static $functions;

	/**
	 * Erstellt und gibt eine Instanz der Klasse zurück.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return object
	 */
	public static function instance() {

		if ( null == self::$instance ) {
			self::$instance = new self;
		}

		return self::$instance;
	}

	/**
	 * PHP constructor.
	 * @since  1.0.0
	 * @access public
	 */
	private function __construct() {

		// Sprachdateien werden eingebunden:
		self::load_textdomain();

		// Erhalte die Einstellungen zum Plugin:
		self::get_options();

		// Aktualisierung des Plugins (ggf):
		self::update_version();

		// Customizer
		add_action( 'customize_preview_init', [ $this, 'customizer_preview' ] );
		add_action( 'customize_register', [ $this, 'customizer_register' ] );
		add_action( 'wp_head', [ $this, 'customizer_css' ] );

		// Enable Builtin Options
		add_theme_support( "post-thumbnails" );
		add_theme_support( "custom-header" );

		// Menüs des Themes
		add_action( "after_setup_theme", [ $this, "register_nav_menus" ] );

		// Sidebars aktivieren
		add_action( 'widgets_init', [ $this, 'sidebars_startpage_init' ] );
		add_action( 'widgets_init', [ $this, 'sidebars_allpages_init' ] );

		// Scripte einbinden
		add_action( "admin_enqueue_scripts", [ $this, "load_admin_scripts" ] );
		add_action( "wp_enqueue_scripts", [ $this, "load_frontend_scripts" ] );

		// Nav Link CSS Manipulation
		add_filter( 'next_posts_link_attributes', [ $this, 'nav_link_attributes' ] );
		add_filter( 'previous_posts_link_attributes', [ $this, "nav_link_attributes" ] );

		// Excerpt Manipulation
		add_filter( 'excerpt_length', [ $this, 'new_excerpt_length' ] );
		add_filter( 'excerpt_more', [ $this, 'new_excerpt_more' ] );

		// Shortcode
		add_shortcode( 'mcc-year', [ $this, "shortcode_year" ] );
		add_shortcode( 'mcc-title', [ $this, "shortcode_blog_title" ] );
		add_shortcode( 'mcc-description', [ $this, "shortcode_blog_description" ] );

		// additional image sizes
		add_image_size( 'mcc-featured-image', 9999, 330 );
		add_image_size( 'mcc-nav-image', 9999, 20 );

		/*
         * Let WordPress manage the document title.
         * By adding theme support, we declare that this theme does not use a
         * hard-coded <title> tag in the document head, and expect WordPress to
         * provide it for us.
         */
		add_theme_support( 'title-tag' );

		/*
         * Switch default core markup for search form, comment form, and comments
         * to output valid HTML5.
         */
		add_theme_support( 'html5', [
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		] );

		/*
         * Enable support for Post Formats.
         *
         * See: https://codex.wordpress.org/Post_Formats
         */
		add_theme_support( 'post-formats', [
			'image',
			'video',
			'gallery',
			'audio',
		] );

		/*
		// Add theme support for Custom Logo.
		add_theme_support( 'custom-logo', array(
			'width'       => 250,
			'height'      => 250,
			'flex-width'  => true,
		) );
        */

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		// Remove image size attributes from post thumbnails
		add_filter( 'post_thumbnail_html', [ $this, 'remove_image_size_attributes' ] );

		// Remove image size attributes from images added to a WordPress post
		add_filter( 'image_send_to_editor', [ $this, 'remove_image_size_attributes' ] );

		// Custom Widgets
		add_action( 'widgets_init', [ $this, 'register_widgets' ] );
	}

	/**
	 * Einbindung der Sprachdateien
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function load_textdomain() {

		load_plugin_textdomain( MCC_THEME_TXT, false, sprintf( '%s/languages/', dirname( plugin_basename( __FILE__ ) ) ) );
	}

	/**
	 * Aktionen bei Plugin Aktivierung
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @param $network_wide boolean Teilt mit, ob Netzwerkweit aktiviert werden soll
	 *
	 * @return void
	 */
	public static function activation( $network_wide ) {

		// Sprachdateien werden eingebunden.
		self::load_textdomain();

		// Überprüft die minimal erforderliche PHP- u. WP-Version.
		self::system_requirements();

		// Aktualisierung des Plugins (ggf).
		self::update_version();
	}

	/**
	 * Aktionen bei Plugin Deaktivierung
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @param $network_wide boolean Teilt mit, ob Netzwerkweit aktiviert werden soll
	 *
	 * @return void
	 */
	public static function deactivation( $network_wide ) {

		// Hier können die Funktionen/Methoden hinzugefügt werden, die
		// bei der Deaktivierung des Plugins aufgerufen werden müssen.

		// Todo: in Optionsseite einstellen, ob löschen oder nicht

		self::delete_options();
	}

	/**
	 * Überprüft die minimal erforderliche PHP- u. WP-Version
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function system_requirements() {

		$error = '';

		if ( version_compare( PHP_VERSION, self::php_version, '<' ) ) {
			$error = sprintf( __( 'MCC-Theme: Your server is running PHP version %s. Please upgrade at least to PHP version %s.', 'cms-basis' ), PHP_VERSION, self::wp_version );
		}

		if ( version_compare( $GLOBALS['wp_version'], self::wp_version, '<' ) ) {
			$error = sprintf( __( 'MCC-Theme: Your Wordpress version is %s. Please upgrade at least to Wordpress version %s.', 'cms-basis' ), $GLOBALS['wp_version'], self::wp_version );
		}

		// Wenn die Überprüfung fehlschlägt, dann wird das Plugin automatisch deaktiviert
		if ( ! empty( $error ) ) {
			deactivate_plugins( plugin_basename( __FILE__ ), false, true );
			// Beende
			wp_die( $error );
		}
	}

	/**
	 * Aktualisiere Plugin, wenn nötig
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	private static function update_version() {

		// Hole evtl. gespeicherte Version Nummer:
		$version_stored = get_option( static::version_option_name, '0' );

		if ( version_compare( $version_stored, static::version, '<' ) ) {
			// Wird durchgeführt wenn das Plugin aktualisiert muss.

			// Todo: In dieser Version ist noch nichts zu tun

			// Speichere aktuelle Versions Nummer:
			update_option( static::version_option_name, static::version );
		}
	}

	/**
	 * Standardeinstellungen definieren
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return array
	 */
	private static function default_options() {

		// Multidimensionales Array ist möglich:
		$options = [];

		return $options;
	}

	/**
	 * Setzt die Einstellungen und Eigenschaften, mit Berücksichtung auf Default Einstellungen
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function get_options() {

		// Default Einstellungen
		$defaults = static::default_options();

		// Einstellungen
		$options = get_option( static::option_name );
		if ( $options === false ) {
			$options = [];
		}

		// Setze fehlende Einstellungen durch Default Einstellungen
		mcc_functions::parse_args_multidim( $options, $defaults );

		// Setze Optionen
		static::$options = $options;

		// Setze Eigenschaften der Klasse
		static::set_class_properties();
	}

	/**
	 * Speichert die Plugin Optionen und frischt die Klassen Eigenschaften auf
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function save_options() {

		update_option( static::option_name, static::$options );

		// Initialisiere Eigenschaften
		static::set_class_properties();
	}

	/**
	 * Löscht alle Einstellungen
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function delete_options() {

		delete_option( static::option_name );
		delete_option( self::version_option_name );
	}

	/**
	 * Setzt die Eigenschaften der Klasse
	 * @since  1.0.0
	 * @access protected
	 * @static
	 * @return void
	 */
	protected static function set_class_properties() {
	}

	/**
	 * Dummy Funktion
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function dummy_callback() {
	}

	/**
	 * Registriere neue Widgets
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_widgets() {

		register_widget( 'mcc_widget_tax_menu' );
	}

	/**
	 * Länge des Excerpts setzen
	 * @since  1.0.0
	 * @access public
	 *
	 * @param $length int Die Wordpress Länge (Nicht genutzt)
	 *
	 * @return int
	 */
	public function new_excerpt_length( $length ) {

		return 30;
	}

	/**
	 * "Excerpt more" string setzen
	 * @since  1.0.0
	 * @access public
	 *
	 * @param $more string Wordpress More (Nicht genutzt)
	 *
	 * @return string
	 */
	public function new_excerpt_more( $more ) {

		global $post;
		$fade_out = "...";
		$content  = "";
		$content  .= " <a href='" . get_permalink( $post->ID ) . "' class='read-more-link'>$fade_out <span class='read-more'></span><span class='read-more-hover' style='display:none;'> </span></a>";

		return $content;
	}

	/**
	 * CSS Klasse für die Navigation durch Seiten setzen
	 * @since  1.0.0
	 * @access public
	 * @return string
	 */
	public function nav_link_attributes() {

		return 'class="wp-link-nav"';
	}

	/**
	 * Die Menü Positionen des Themes bekannt geben
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function register_nav_menus() {

		register_nav_menu( "meta-main-nav", "Meta Navigation Main" );
		register_nav_menu( "meta-top-nav", "Meta Navigation Top" );
		register_nav_menu( "meta-bottom-nav", "Meta Navigation Bottom" );
	}

	/**
	 * Scripte und CSS laden - Admin Bereich
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function load_admin_scripts() {
	}

	/**
	 * Scripte und CSS laden - Frontend Bereich
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function load_frontend_scripts() {

		// Externe:
		//wp_enqueue_style( "mcc-theme-tether", get_template_directory_uri() . "/external/tether/dist/css/tether.css", [], '1.3.3', 'all' );
		wp_enqueue_style( "mcc-theme-jquery-ui", get_template_directory_uri() . "/external/jquery-ui/jquery-ui.min.css", [], '1.12.1', 'all' );
		wp_enqueue_style( "mcc-theme-bootstrap", get_template_directory_uri() . "/external/bootstrap/css/bootstrap.min.css", [], '4.4.1', 'all' );
		wp_enqueue_style( "mcc-theme-offcanvas", get_template_directory_uri() . "/external/bootstrap-offcanvas/dist/css/bootstrap.offcanvas.css", [], '2.5.2', 'all' );
		//wp_enqueue_style( "mcc-theme-hovercss", get_template_directory_uri() . "/external/hovercss/hover-min.css", [], '2.5.2', 'all' );
		if ( ! is_customize_preview() ) {
			wp_enqueue_script( "mcc-theme-jquery", get_template_directory_uri() . "/external/jquery/jquery.min.js", [], "3.3.1", true );
		}
		wp_enqueue_script( "mcc-theme-popper", get_template_directory_uri() . "/external/popper/popper.min.js", [], "1.14.3", true );
		//wp_enqueue_script( "mcc-theme-tether", get_template_directory_uri() . "/external/tether/dist/js/tether.min.js", [], "1.3.3", true );
		wp_enqueue_script( "mcc-theme-jquery-ui", get_template_directory_uri() . "/external/jquery-ui/jquery-ui.min.js", [], "3.2.1", true );
		wp_enqueue_script( "mcc-theme-bootstrap", get_template_directory_uri() . "/external/bootstrap/js/bootstrap.min.js", [], "4.4.1", true );
		wp_enqueue_script( "mcc-theme-offcanvas", get_template_directory_uri() . "/external/bootstrap-offcanvas/dist/js/bootstrap.offcanvas.js", [], "2.5.2", true );

		//wp_enqueue_script( "mcc-theme-jquery-sticky", get_template_directory_uri() . "/external/bootstrap/jquery.sticky.js", array( "jquery" ), "1.0.0", true );
		//wp_enqueue_script( "mcc-theme-jquery-scrolly", get_template_directory_uri() . "/external/bootstrap/jquery.scrolly.min.js", array( "jquery" ), "1.0.0", true );
		//wp_enqueue_script( "mcc-theme-bootstrap-offcanvas", get_template_directory_uri() . "/external/bootstrap-offcanvas/dist/js/bootstrap.offcanvas.js", [ "jquery" ], "2.4.0", true );
		//wp_enqueue_style( "mcc-theme-bootstrap-offcanvas", get_template_directory_uri() . "/external/bootstrap-offcanvas/dist/css/bootstrap.offcanvas.css", [], "2.4.0", 'all' );

		// Eigene:
		wp_enqueue_style( "mcc-theme-front", get_template_directory_uri() . "/css/front.css", [], static::version, 'all' );
        wp_enqueue_style( "mcc-font-awesome", get_template_directory_uri() . "/css/all.min.css", [], "5.2.0", 'all' );
		wp_enqueue_script( "mcc-theme-js", get_template_directory_uri() . "/js/mcc.js", [], static::version, true );
	}

	/**
	 * Sidebars für die Startseite aktivieren
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function sidebars_startpage_init() {

		register_sidebar( [
			'name'          => __( 'Frontpage main content 1', MCC_THEME_TXT ),
			'id'            => 'mcc-frontpage-main-content-1',
			'description'   => __( 'Sidebar displayed on Frontpage in the main content area', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Frontpage main content 2', MCC_THEME_TXT ),
			'id'            => 'mcc-frontpage-main-content-2',
			'description'   => __( 'Sidebar displayed on Frontpage in the main content area', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Frontpage main content 3', MCC_THEME_TXT ),
			'id'            => 'mcc-frontpage-main-content-3',
			'description'   => __( 'Sidebar displayed on Frontpage in the main content area', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );
	}

	/**
	 * Sidebars für alle Seiten aktivieren
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function sidebars_allpages_init() {

		register_sidebar( [
			'name'          => __( 'Content 1', MCC_THEME_TXT ),
			'id'            => 'mcc-main-content-1',
			'description'   => __( 'Sidebar displayed in Pages', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Content 2', MCC_THEME_TXT ),
			'id'            => 'mcc-main-content-2',
			'description'   => __( 'Sidebar displayed in Pages', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Content 3', MCC_THEME_TXT ),
			'id'            => 'mcc-main-content-3',
			'description'   => __( 'Sidebar displayed in Pages', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Footer 1', MCC_THEME_TXT ),
			'id'            => 'mcc-footer-1',
			'description'   => __( 'Sidebar displayed in Footer', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Footer 2', MCC_THEME_TXT ),
			'id'            => 'mcc-footer-2',
			'description'   => __( 'Sidebar displayed in Footer', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );

		register_sidebar( [
			'name'          => __( 'Footer 3', MCC_THEME_TXT ),
			'id'            => 'mcc-footer-3',
			'description'   => __( 'Sidebar displayed in Footer', MCC_THEME_TXT ),
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1 class="widget-title">',
			'after_title'   => '</h1>',
		] );
	}



	////////////////////////////////////////
	// START Custom Shortcodes
	////////////////////////////////////////

	/**
	 * Shortcode: Aktuelles Jahr anzeigen
	 * @since  1.0.0
	 * @access public
	 *
	 * @param array  $atts    Attribute
	 * @param string $content Inhalt
	 * @param string $tag     Tags
	 *
	 * @return string
	 */
	public function shortcode_year( $atts = [], $content = null, $tag = '' ) {

		return date( "Y" );
	}

	/**
	 * Shortcode: Blog Titel anzeigen
	 * @since  1.0.0
	 * @access public
	 *
	 * @param array  $atts    Attribute
	 * @param string $content Inhalt
	 * @param string $tag     Tags
	 *
	 * @return string
	 */
	public function shortcode_blog_title( $atts = [], $content = null, $tag = '' ) {

		return get_bloginfo( "name" );
	}

	/**
	 * Shortcode: Beschreibung anzeigen
	 * @since  1.0.0
	 * @access public
	 *
	 * @param array  $atts    Attribute
	 * @param string $content Inhalt
	 * @param string $tag     Tags
	 *
	 * @return string
	 */
	public function shortcode_blog_description( $atts = [], $content = null, $tag = '' ) {

		return get_bloginfo( "description" );
	}



	////////////////////////////////////////
	// END Custom Shortcodes
	////////////////////////////////////////

	/**
	 * Bindet das Navigationstemplate ein
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function get_comments_navigation() {

		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ):

			get_template_part( 'templates/nav', 'comment' );

		endif;
	}

	/**
	 * Customizer Preview
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function customizer_css() {

		$css = "";

		$background_color_meta_top           = get_theme_mod( 'background_color_meta_top_setting', $this->customizer_defaults["background_color_meta_top_setting"] );
		$background_color_meta_top_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_meta_top );
		$background_color_opacity_meta_top   = get_theme_mod( 'background_color_opacity_meta_top_setting', $this->customizer_defaults["background_color_opacity_meta_top_setting"] );
		if ( is_numeric( $background_color_opacity_meta_top ) ) {
			$background_color_meta_top_css = "rgba(" . $background_color_meta_top_rgb_array["r"] . ", " . $background_color_meta_top_rgb_array["g"] . ", " . $background_color_meta_top_rgb_array["b"] . ", $background_color_opacity_meta_top)";
		} else {
			$background_color_meta_top_css = "rgba(" . $background_color_meta_top_rgb_array["r"] . ", " . $background_color_meta_top_rgb_array["g"] . ", " . $background_color_meta_top_rgb_array["b"] . ")";
		}

		$border_color_meta_top           = get_theme_mod( 'border_color_meta_top_setting', $this->customizer_defaults["border_color_meta_top_setting"] );
		$border_color_meta_top_rgb_array = mcc_functions::get_rgb_from_hex( $border_color_meta_top );
		$border_color_opacity_meta_top   = get_theme_mod( 'border_color_opacity_meta_top_setting', $this->customizer_defaults["border_color_opacity_meta_top_setting"] );
		if ( is_numeric( $border_color_opacity_meta_top ) ) {
			$border_color_meta_top_css = "rgba(" . $border_color_meta_top_rgb_array["r"] . ", " . $border_color_meta_top_rgb_array["g"] . ", " . $border_color_meta_top_rgb_array["b"] . ", $border_color_opacity_meta_top)";
		} else {
			$border_color_meta_top_css = "rgba(" . $border_color_meta_top_rgb_array["r"] . ", " . $border_color_meta_top_rgb_array["g"] . ", " . $border_color_meta_top_rgb_array["b"] . ")";
		}

		$background_color_meta_top_submenu           = get_theme_mod( 'submenu_background_color_meta_top_setting', $this->customizer_defaults["submenu_background_color_meta_top_setting"] );
		$background_color_meta_top_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $background_color_meta_top_submenu );
		$background_color_opacity_meta_top_submenu   = get_theme_mod( 'submenu_background_color_opacity_meta_top_setting', $this->customizer_defaults["submenu_background_color_opacity_meta_top_setting"] );
		if ( is_numeric( $background_color_opacity_meta_top_submenu ) ) {
			$background_color_meta_top_css_submenu = "rgba(" . $background_color_meta_top_rgb_array_submenu["r"] . ", " . $background_color_meta_top_rgb_array_submenu["g"] . ", " . $background_color_meta_top_rgb_array_submenu["b"] . ", $background_color_opacity_meta_top_submenu)";
		} else {
			$background_color_meta_top_css_submenu = "rgba(" . $background_color_meta_top_rgb_array_submenu["r"] . ", " . $background_color_meta_top_rgb_array_submenu["g"] . ", " . $background_color_meta_top_rgb_array_submenu["b"] . ")";
		}

		$border_color_meta_top_submenu           = get_theme_mod( 'submenu_border_color_meta_top_setting', $this->customizer_defaults["submenu_border_color_meta_top_setting"] );
		$border_color_meta_top_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $border_color_meta_top_submenu );
		$border_color_opacity_meta_top_submenu   = get_theme_mod( 'submenu_border_color_opacity_meta_top_setting', $this->customizer_defaults["submenu_border_color_opacity_meta_top_setting"] );
		if ( is_numeric( $border_color_opacity_meta_top_submenu ) ) {
			$border_color_meta_top_css_submenu = "rgba(" . $border_color_meta_top_rgb_array_submenu["r"] . ", " . $border_color_meta_top_rgb_array_submenu["g"] . ", " . $border_color_meta_top_rgb_array_submenu["b"] . ", $border_color_opacity_meta_top_submenu)";
		} else {
			$border_color_meta_top_css_submenu = "rgba(" . $border_color_meta_top_rgb_array_submenu["r"] . ", " . $border_color_meta_top_rgb_array_submenu["g"] . ", " . $border_color_meta_top_rgb_array_submenu["b"] . ")";
		}

		$hover_color_meta_top_submenu           = get_theme_mod( 'submenu_background_hover_color_meta_top_setting', $this->customizer_defaults["submenu_background_hover_color_meta_top_setting"] );
		$hover_color_meta_top_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $hover_color_meta_top_submenu );
		$hover_color_opacity_meta_top_submenu   = get_theme_mod( 'submenu_background_hover_color_opacity_meta_top_setting', $this->customizer_defaults["submenu_background_hover_color_opacity_meta_top_setting"] );
		if ( is_numeric( $hover_color_opacity_meta_top_submenu ) ) {
			$hover_color_meta_top_css_submenu = "rgba(" . $hover_color_meta_top_rgb_array_submenu["r"] . ", " . $hover_color_meta_top_rgb_array_submenu["g"] . ", " . $hover_color_meta_top_rgb_array_submenu["b"] . ", $hover_color_opacity_meta_top_submenu)";
		} else {
			$hover_color_meta_top_css_submenu = "rgba(" . $hover_color_meta_top_rgb_array_submenu["r"] . ", " . $hover_color_meta_top_rgb_array_submenu["g"] . ", " . $hover_color_meta_top_rgb_array_submenu["b"] . ")";
		}

		$background_image_id_meta_top = get_theme_mod( 'background_image_meta_top_setting', $this->customizer_defaults["background_image_meta_top_setting"] );
		if ( ! empty( $background_image_id_meta_top ) ) {
			$background_image_src_meta_top = wp_get_attachment_image_src( $background_image_id_meta_top, "full" );
			if ( $background_image_src_meta_top[0] ) {
				$css .= "#meta-top-wrapper, #meta-bottom-wrapper { background-image: url('" . $background_image_src_meta_top["0"] . "'); }";
				$css .= "#meta-top-nav-wrapper .collapse.show ul, #meta-bottom-wrapper .collapse.show ul { background-image: url('" . $background_image_src_meta_top["0"] . "'); }";
				$css .= "#meta-top-nav-wrapper .collapsing ul, #meta-bottom-wrapper .collapsing ul { background-image: url('" . $background_image_src_meta_top["0"] . "'); }";
			}
		}
		$background_image_id_meta_top_submenu = get_theme_mod( 'submenu_background_image_meta_top_setting', $this->customizer_defaults["submenu_background_image_meta_top_setting"] );
		if ( ! empty( $background_image_id_meta_top_submenu ) ) {
			$background_image_src_meta_top_submenu = wp_get_attachment_image_src( $background_image_id_meta_top_submenu, "full" );
			if ( $background_image_src_meta_top_submenu[0] ) {
				$css .= "#meta-top-wrapper .dropdown-menu, #meta-bottom-wrapper .dropdown-menu { background-image: url('" . $background_image_src_meta_top_submenu["0"] . "'); }";
			}
		}

		$background_size_meta_top = get_theme_mod( 'background_image_cover_meta_top_setting', $this->customizer_defaults["background_image_cover_meta_top_setting"] );
		if ( $background_size_meta_top ) {
			$background_size_css_meta_top = "cover";
		} else {
			$background_size_css_meta_top = "auto";
		}
		$background_size_meta_top_submenu = get_theme_mod( 'submenu_background_image_cover_meta_top_setting', $this->customizer_defaults["submenu_background_image_cover_meta_top_setting"] );
		if ( $background_size_meta_top_submenu ) {
			$background_size_css_meta_top_submenu = "cover";
		} else {
			$background_size_css_meta_top_submenu = "auto";
		}
		/////// Meta Main Start
		$background_color_meta_main           = get_theme_mod( 'background_color_meta_main_setting', $this->customizer_defaults["background_color_meta_main_setting"] );
		$background_color_meta_main_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_meta_main );
		$background_color_opacity_meta_main   = get_theme_mod( 'background_color_opacity_meta_main_setting', $this->customizer_defaults["background_color_opacity_meta_main_setting"] );
		if ( is_numeric( $background_color_opacity_meta_main ) ) {
			$background_color_meta_main_css = "rgba(" . $background_color_meta_main_rgb_array["r"] . ", " . $background_color_meta_main_rgb_array["g"] . ", " . $background_color_meta_main_rgb_array["b"] . ", $background_color_opacity_meta_main)";
		} else {
			$background_color_meta_main_css = "rgba(" . $background_color_meta_main_rgb_array["r"] . ", " . $background_color_meta_main_rgb_array["g"] . ", " . $background_color_meta_main_rgb_array["b"] . ")";
		}

		$border_color_meta_main           = get_theme_mod( 'border_color_meta_main_setting', $this->customizer_defaults["border_color_meta_main_setting"] );
		$border_color_meta_main_rgb_array = mcc_functions::get_rgb_from_hex( $border_color_meta_main );
		$border_color_opacity_meta_main   = get_theme_mod( 'border_color_opacity_meta_main_setting', $this->customizer_defaults["border_color_opacity_meta_main_setting"] );
		if ( is_numeric( $border_color_opacity_meta_main ) ) {
			$border_color_meta_main_css = "rgba(" . $border_color_meta_main_rgb_array["r"] . ", " . $border_color_meta_main_rgb_array["g"] . ", " . $border_color_meta_main_rgb_array["b"] . ", $border_color_opacity_meta_main)";
		} else {
			$border_color_meta_main_css = "rgba(" . $border_color_meta_main_rgb_array["r"] . ", " . $border_color_meta_main_rgb_array["g"] . ", " . $border_color_meta_main_rgb_array["b"] . ")";
		}

		$background_color_meta_main_submenu           = get_theme_mod( 'submenu_background_color_meta_main_setting', $this->customizer_defaults["submenu_background_color_meta_main_setting"] );
		$background_color_meta_main_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $background_color_meta_main_submenu );
		$background_color_opacity_meta_main_submenu   = get_theme_mod( 'submenu_background_color_opacity_meta_main_setting', $this->customizer_defaults["submenu_background_color_opacity_meta_main_setting"] );
		if ( is_numeric( $background_color_opacity_meta_main_submenu ) ) {
			$background_color_meta_main_css_submenu = "rgba(" . $background_color_meta_main_rgb_array_submenu["r"] . ", " . $background_color_meta_main_rgb_array_submenu["g"] . ", " . $background_color_meta_main_rgb_array_submenu["b"] . ", $background_color_opacity_meta_main_submenu)";
		} else {
			$background_color_meta_main_css_submenu = "rgba(" . $background_color_meta_main_rgb_array_submenu["r"] . ", " . $background_color_meta_main_rgb_array_submenu["g"] . ", " . $background_color_meta_main_rgb_array_submenu["b"] . ")";
		}

		$border_color_meta_main_submenu           = get_theme_mod( 'submenu_border_color_meta_main_setting', $this->customizer_defaults["submenu_border_color_meta_main_setting"] );
		$border_color_meta_main_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $border_color_meta_main_submenu );
		$border_color_opacity_meta_main_submenu   = get_theme_mod( 'submenu_border_color_opacity_meta_main_setting', $this->customizer_defaults["submenu_border_color_opacity_meta_main_setting"] );
		if ( is_numeric( $border_color_opacity_meta_main_submenu ) ) {
			$border_color_meta_main_css_submenu = "rgba(" . $border_color_meta_main_rgb_array_submenu["r"] . ", " . $border_color_meta_main_rgb_array_submenu["g"] . ", " . $border_color_meta_main_rgb_array_submenu["b"] . ", $border_color_opacity_meta_main_submenu)";
		} else {
			$border_color_meta_main_css_submenu = "rgba(" . $border_color_meta_main_rgb_array_submenu["r"] . ", " . $border_color_meta_main_rgb_array_submenu["g"] . ", " . $border_color_meta_main_rgb_array_submenu["b"] . ")";
		}

		$divider_color_meta_main           = get_theme_mod( 'divider_color_meta_main_setting', $this->customizer_defaults["divider_color_meta_main_setting"] );
		$divider_color_meta_main_rgb_array = mcc_functions::get_rgb_from_hex( $divider_color_meta_main );
		$divider_color_opacity_meta_main   = get_theme_mod( 'divider_color_opacity_meta_main_setting', $this->customizer_defaults["divider_color_opacity_meta_main_setting"] );
		if ( is_numeric( $divider_color_opacity_meta_main ) ) {
			$divider_color_meta_main_css = "rgba(" . $divider_color_meta_main_rgb_array["r"] . ", " . $divider_color_meta_main_rgb_array["g"] . ", " . $divider_color_meta_main_rgb_array["b"] . ", $divider_color_opacity_meta_main)";
		} else {
			$divider_color_meta_main_css = "rgba(" . $divider_color_meta_main_rgb_array["r"] . ", " . $divider_color_meta_main_rgb_array["g"] . ", " . $divider_color_meta_main_rgb_array["b"] . ")";
		}

		$hover_color_meta_main_submenu           = get_theme_mod( 'submenu_background_hover_color_meta_main_setting', $this->customizer_defaults["submenu_background_hover_color_meta_main_setting"] );
		$hover_color_meta_main_rgb_array_submenu = mcc_functions::get_rgb_from_hex( $hover_color_meta_main_submenu );
		$hover_color_opacity_meta_main_submenu   = get_theme_mod( 'submenu_background_hover_color_opacity_meta_main_setting', $this->customizer_defaults["submenu_background_hover_color_opacity_meta_main_setting"] );
		if ( is_numeric( $hover_color_opacity_meta_main_submenu ) ) {
			$hover_color_meta_main_css_submenu = "rgba(" . $hover_color_meta_main_rgb_array_submenu["r"] . ", " . $hover_color_meta_main_rgb_array_submenu["g"] . ", " . $hover_color_meta_main_rgb_array_submenu["b"] . ", $hover_color_opacity_meta_main_submenu)";
		} else {
			$hover_color_meta_main_css_submenu = "rgba(" . $hover_color_meta_main_rgb_array_submenu["r"] . ", " . $hover_color_meta_main_rgb_array_submenu["g"] . ", " . $hover_color_meta_main_rgb_array_submenu["b"] . ")";
		}

		$background_image_id_meta_main = get_theme_mod( 'background_image_meta_main_setting', $this->customizer_defaults["background_image_meta_main_setting"] );
		if ( ! empty( $background_image_id_meta_main ) ) {
			$background_image_src_meta_main = wp_get_attachment_image_src( $background_image_id_meta_main, "full" );
			if ( $background_image_src_meta_main[0] ) {
				$css .= "#meta-main-wrapper{ background-image: url('" . $background_image_src_meta_main["0"] . "'); }";
				$css .= "#meta-main-nav-wrapper .collapse.show ul { background-image: url('" . $background_image_src_meta_main["0"] . "'); }";
				$css .= "#meta-main-nav-wrapper .collapsing ul { background-image: url('" . $background_image_src_meta_main["0"] . "'); }";
			}
		}
		$background_image_id_meta_main_submenu = get_theme_mod( 'submenu_background_image_meta_main_setting', $this->customizer_defaults["submenu_background_image_meta_main_setting"] );
		if ( ! empty( $background_image_id_meta_main_submenu ) ) {
			$background_image_src_meta_main_submenu = wp_get_attachment_image_src( $background_image_id_meta_main_submenu, "full" );
			if ( $background_image_src_meta_main_submenu[0] ) {
				$css .= "#meta-main-wrapper .dropdown-menu { background-image: url('" . $background_image_src_meta_main_submenu["0"] . "'); }";
			}
		}

		$background_size_meta_main = get_theme_mod( 'background_image_cover_meta_main_setting', $this->customizer_defaults["background_image_cover_meta_main_setting"] );
		if ( $background_size_meta_main ) {
			$background_size_css_meta_main = "cover";
		} else {
			$background_size_css_meta_main = "auto";
		}
		$background_size_meta_main_submenu = get_theme_mod( 'submenu_background_image_cover_meta_main_setting', $this->customizer_defaults["submenu_background_image_cover_meta_main_setting"] );
		if ( $background_size_meta_main_submenu ) {
			$background_size_css_meta_main_submenu = "cover";
		} else {
			$background_size_css_meta_main_submenu = "auto";
		}
		/// Meta Main End

		/// Footer STart
		$background_color_footer           = get_theme_mod( 'background_color_footer_setting', $this->customizer_defaults["background_color_footer_setting"] );
		$background_color_footer_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_footer );
		$background_color_opacity_footer   = 1;
		if ( is_numeric( $background_color_opacity_footer ) ) {
			$background_color_footer_css = "rgba(" . $background_color_footer_rgb_array["r"] . ", " . $background_color_footer_rgb_array["g"] . ", " . $background_color_footer_rgb_array["b"] . ", $background_color_opacity_footer)";
		} else {
			$background_color_footer_css = "rgba(" . $background_color_footer_rgb_array["r"] . ", " . $background_color_footer_rgb_array["g"] . ", " . $background_color_footer_rgb_array["b"] . ")";
		}
		$background_image_id_footer = get_theme_mod( 'background_image_footer_setting', $this->customizer_defaults["background_image_footer_setting"] );
		if ( ! empty( $background_image_id_footer ) ) {
			$background_image_src_footer = wp_get_attachment_image_src( $background_image_id_footer, "full" );
			if ( $background_image_src_footer[0] ) {
				$css .= "body{ background-image: url('" . $background_image_src_footer["0"] . "'); }";
			}
		}

		$background_size_footer = get_theme_mod( 'background_image_cover_footer_setting', $this->customizer_defaults["background_image_cover_footer_setting"] );
		if ( $background_size_footer ) {
			$background_size_css_footer = "cover";
		} else {
			$background_size_css_footer = "auto";
		}
		$border_color_footer           = get_theme_mod( 'border_color_footer_setting', $this->customizer_defaults["border_color_footer_setting"] );
		$border_color_footer_rgb_array = mcc_functions::get_rgb_from_hex( $border_color_footer );
		$border_color_opacity_footer   = get_theme_mod( 'border_color_opacity_footer_setting', $this->customizer_defaults["border_color_opacity_footer_setting"] );
		if ( is_numeric( $border_color_opacity_footer ) ) {
			$border_color_footer_css = "rgba(" . $border_color_footer_rgb_array["r"] . ", " . $border_color_footer_rgb_array["g"] . ", " . $border_color_footer_rgb_array["b"] . ", $border_color_opacity_footer)";
		} else {
			$border_color_footer_css = "rgba(" . $border_color_footer_rgb_array["r"] . ", " . $border_color_footer_rgb_array["g"] . ", " . $border_color_footer_rgb_array["b"] . ")";
		}
		/// Footer End
		/// Content STart 
		$background_color_content           = get_theme_mod( 'background_color_content_setting', $this->customizer_defaults["background_color_content_setting"] );
		$background_color_content_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_content );
		$background_color_opacity_content   = 1;
		if ( is_numeric( $background_color_opacity_content ) ) {
			$background_color_content_css = "rgba(" . $background_color_content_rgb_array["r"] . ", " . $background_color_content_rgb_array["g"] . ", " . $background_color_content_rgb_array["b"] . ", $background_color_opacity_content)";
		} else {
			$background_color_content_css = "rgba(" . $background_color_content_rgb_array["r"] . ", " . $background_color_content_rgb_array["g"] . ", " . $background_color_content_rgb_array["b"] . ")";
		}
		$background_image_id_content = get_theme_mod( 'background_image_content_setting', $this->customizer_defaults["background_image_content_setting"] );
		if ( ! empty( $background_image_id_content ) ) {
			$background_image_src_content = wp_get_attachment_image_src( $background_image_id_content, "full" );
			if ( $background_image_src_content[0] ) {
				$css .= "#content-wrapper { background-image: url('" . $background_image_src_content["0"] . "'); }";
			}
		}
		$background_size_content = get_theme_mod( 'background_image_cover_content_setting', $this->customizer_defaults["background_image_cover_content_setting"] );
		if ( $background_size_content ) {
			$background_size_css_content = "cover";
		} else {
			$background_size_css_content = "auto";
		}
		$border_color_content           = get_theme_mod( 'border_color_content_setting', $this->customizer_defaults["border_color_content_setting"] );
		$border_color_content_rgb_array = mcc_functions::get_rgb_from_hex( $border_color_content );
		$border_color_opacity_content   = get_theme_mod( 'border_color_opacity_content_setting', $this->customizer_defaults["border_color_opacity_content_setting"] );
		if ( is_numeric( $border_color_opacity_content ) ) {
			$border_color_content_css = "rgba(" . $border_color_content_rgb_array["r"] . ", " . $border_color_content_rgb_array["g"] . ", " . $border_color_content_rgb_array["b"] . ", $border_color_opacity_content)";
		} else {
			$border_color_content_css = "rgba(" . $border_color_content_rgb_array["r"] . ", " . $border_color_content_rgb_array["g"] . ", " . $border_color_content_rgb_array["b"] . ")";
		}

		$background_color_content_paging           = get_theme_mod( 'background_color_content_paging_setting', $this->customizer_defaults["background_color_content_paging_setting"] );
		$background_color_content_paging_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_content_paging );
		$background_color_opacity_content_paging   = get_theme_mod( 'background_color_content_paging_opacity_setting', $this->customizer_defaults["background_color_content_paging_opacity_setting"] );;
		if ( is_numeric( $background_color_opacity_content_paging ) ) {
			$background_color_content_paging_css = "rgba(" . $background_color_content_paging_rgb_array["r"] . ", " . $background_color_content_paging_rgb_array["g"] . ", " . $background_color_content_paging_rgb_array["b"] . ", $background_color_opacity_content_paging)";
		} else {
			$background_color_content_paging_css = "rgba(" . $background_color_content_paging_rgb_array["r"] . ", " . $background_color_content_paging_rgb_array["g"] . ", " . $background_color_content_paging_rgb_array["b"] . ")";
		}

		/// Content End /// 
		/// Full Header Background
		$background_color_full_header           = get_theme_mod( 'background_color_full_header_setting', $this->customizer_defaults["background_color_full_header_setting"] );
		$background_color_full_header_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_full_header );
		$background_color_opacity_full_header   = 1;
		if ( is_numeric( $background_color_opacity_full_header ) ) {
			$background_color_full_header_css = "rgba(" . $background_color_full_header_rgb_array["r"] . ", " . $background_color_full_header_rgb_array["g"] . ", " . $background_color_full_header_rgb_array["b"] . ", $background_color_opacity_full_header)";
		} else {
			$background_color_full_header_css = "rgba(" . $background_color_full_header_rgb_array["r"] . ", " . $background_color_full_header_rgb_array["g"] . ", " . $background_color_full_header_rgb_array["b"] . ")";
		}
		$background_image_id_full_header = get_theme_mod( 'background_image_full_header_setting', $this->customizer_defaults["background_image_full_header_setting"] );
		if ( ! empty( $background_image_id_full_header ) ) {
			$background_image_src_full_header = wp_get_attachment_image_src( $background_image_id_full_header, "full" );
			if ( $background_image_src_full_header[0] ) {
				$css .= "#full-header-wrapper { background-image: url('" . $background_image_src_full_header["0"] . "'); }";
			}
		}
		$background_size_full_header = get_theme_mod( 'background_image_cover_full_header_setting', $this->customizer_defaults["background_image_cover_full_header_setting"] );
		if ( $background_size_full_header ) {
			$background_size_css_full_header = "cover";
		} else {
			$background_size_css_full_header = "auto";
		}

		/// Full Header Beckground End
		$showhide_metatop = get_theme_mod( 'showhide_metatop_setting', $this->customizer_defaults["showhide_metatop_setting"] );
		if ( $showhide_metatop ) {
			$showhide_metatop_css = "block";
		} else {
			$showhide_metatop_css = "none";
		}

		$showhide_header = get_theme_mod( 'showhide_header_setting', $this->customizer_defaults["showhide_header_setting"] );
		if ( $showhide_header ) {
			$showhide_header_css = "flex";
		} else {
			$showhide_header_css = "none";
		}

		$showhide_metamain = get_theme_mod( 'showhide_metamain_setting', $this->customizer_defaults["showhide_metamain_setting"] );
		if ( $showhide_metamain ) {
			$showhide_metamain_css = "block";
		} else {
			$showhide_metamain_css = "none";
		}

		$showhide_metabottom = get_theme_mod( 'showhide_metabottom_setting', $this->customizer_defaults["showhide_metabottom_setting"] );
		if ( $showhide_metabottom ) {
			$showhide_metabottom_css = "block";
		} else {
			$showhide_metabottom_css = "none";
		}

		$showhide_footer = get_theme_mod( 'showhide_footer_setting', $this->customizer_defaults["showhide_footer_setting"] );
		if ( $showhide_footer ) {
			$showhide_footer_css = "block";
		} else {
			$showhide_footer_css = "none";
		}

		$showhide_content = get_theme_mod( 'showhide_content_setting', $this->customizer_defaults["showhide_content_setting"] );
		if ( $showhide_content ) {
			$showhide_content_css = "block";
		} else {
			$showhide_content_css = "none";
		}

		$input_search_border_color_meta_top_rgb_array    = mcc_functions::get_rgb_from_hex( get_theme_mod( 'font_color_hover_meta_top_setting', $this->customizer_defaults["font_color_hover_meta_top_setting"] ) );
		$input_search_border_color_meta_top_css          = "rgba(" . $input_search_border_color_meta_top_rgb_array["r"] . ", " . $input_search_border_color_meta_top_rgb_array["g"] . ", " . $input_search_border_color_meta_top_rgb_array["b"] . ", 0.8)";
		$input_search_border_shadow_2_color_meta_top_css = "rgba(" . $input_search_border_color_meta_top_rgb_array["r"] . ", " . $input_search_border_color_meta_top_rgb_array["g"] . ", " . $input_search_border_color_meta_top_rgb_array["b"] . ", 0.6)";
		$input_search_border_shadow_1_color_meta_top_css = "rgba(0, 0, 0, 0.075)";

		$background_color_header           = get_theme_mod( 'background_color_header_setting', $this->customizer_defaults["background_color_header_setting"] );
		$background_color_header_rgb_array = mcc_functions::get_rgb_from_hex( $background_color_header );
		$background_color_opacity_header   = get_theme_mod( 'background_color_opacity_header_setting', $this->customizer_defaults["background_color_opacity_header_setting"] );
		if ( is_numeric( $background_color_opacity_header ) ) {
			$background_color_header_css = "rgba(" . $background_color_header_rgb_array["r"] . ", " . $background_color_header_rgb_array["g"] . ", " . $background_color_header_rgb_array["b"] . ", $background_color_opacity_header)";
		} else {
			$background_color_header_css = "rgba(" . $background_color_header_rgb_array["r"] . ", " . $background_color_header_rgb_array["g"] . ", " . $background_color_header_rgb_array["b"] . ")";
		}

		$border_color_header           = get_theme_mod( 'border_color_header_setting', $this->customizer_defaults["border_color_header_setting"] );
		$border_color_header_rgb_array = mcc_functions::get_rgb_from_hex( $border_color_header );
		$border_color_opacity_header   = get_theme_mod( 'border_color_opacity_header_setting', $this->customizer_defaults["border_color_opacity_header_setting"] );
		if ( is_numeric( $border_color_opacity_header ) ) {
			$border_color_header_css = "rgba(" . $border_color_header_rgb_array["r"] . ", " . $border_color_header_rgb_array["g"] . ", " . $border_color_header_rgb_array["b"] . ", $border_color_opacity_header)";
		} else {
			$border_color_header_css = "rgba(" . $border_color_header_rgb_array["r"] . ", " . $border_color_header_rgb_array["g"] . ", " . $border_color_header_rgb_array["b"] . ")";
		}
		$background_image_id_header = get_theme_mod( 'background_image_header_setting', $this->customizer_defaults["background_image_header_setting"] );
		if ( ! empty( $background_image_id_header ) ) {
			$background_image_src_header = wp_get_attachment_image_src( $background_image_id_header, "full" );
			if ( $background_image_src_header[0] ) {
				$css .= ".header-wrapper { background-image: url('" . $background_image_src_header["0"] . "'); }";
			}
		}
		$background_size_header = get_theme_mod( 'background_image_cover_header_setting', $this->customizer_defaults["background_image_cover_header_setting"] );
		if ( $background_size_header ) {
			$background_size_css_header = "cover";
		} else {
			$background_size_css_header = "auto";
		}


        $header_content_center = get_theme_mod( 'header_center_content_setting', $this->customizer_defaults["header_center_content_setting"] );
        if ( $header_content_center ) {
            $header_content_center_content = "center";
        } else {
            $header_content_center_content = "left";
        }

        $header_page_content_center = get_theme_mod( 'header_page_center_content_setting', $this->customizer_defaults["header_page_center_content_setting"] );
        if ( $header_page_content_center ) {
            $header_page_content_center_content = "center";
        } else {
            $header_page_content_center_content = "left";
        }

        $content_content_center = get_theme_mod( 'content_center_content_setting', $this->customizer_defaults["content_center_content_setting"] );
        if ( $content_content_center ) {
            $content_content_center_content = "center";
        } else {
            $content_content_center_content = "left";
        }

        $article_thumbnail_header_show = get_theme_mod( 'article_thumbnail_header_show_setting', $this->customizer_defaults["article_thumbnail_header_show_setting"] );
        if ( $article_thumbnail_header_show ) {
            $article_thumbnail_header_show_content = "display:block";
        } else {
            $article_thumbnail_header_show_content = "display:none";
        }

        $article_preview_thumbnail_header_show = get_theme_mod( 'article_preview_thumbnail_header_show_setting', $this->customizer_defaults["article_preview_thumbnail_header_show_setting"] );
        if ( $article_preview_thumbnail_header_show ) {
            $article_preview_thumbnail_header_show_content = "display:block";
            $article_preview_thumbnail_header_show_extra_content = "";
        } else {
            $article_preview_thumbnail_header_show_content = "display:none";
            $article_preview_thumbnail_header_show_extra_content = ".entry-col{ width: 100%;  max-width: 100%;}\n";
        }


        $article_meta_top_show = get_theme_mod( 'article_meta_top_show_setting', $this->customizer_defaults["article_meta_top_show_setting"] );
        if ( $article_meta_top_show ) {
            $article_meta_top_show_content = "display:block";
        } else {
            $article_meta_top_show_content = "display:none";
        }

		$style = "<style>
            $css;
            #meta-top-wrapper, #meta-bottom-wrapper { display: $showhide_metatop_css; }
            #meta-bottom-wrapper { display:  $showhide_metabottom_css; }
            #meta-top-wrapper, #meta-top-nav-wrapper .collapse.show ul, #meta-top-nav-wrapper .collapsing ul, #meta-bottom-wrapper, #meta-bottom-nav-wrapper .collapse.show ul, #meta-bottom-nav-wrapper .collapsing ul { background-color:  $background_color_meta_top_css; }
            #meta-top-wrapper, #meta-top-nav-wrapper .collapse.show ul, #meta-top-nav-wrapper .collapsing ul, #meta-bottom-wrapper, #meta-bottom-nav-wrapper .collapse.show ul, #meta-bottom-nav-wrapper .collapsing ul { background-size:  $background_size_css_meta_top; }
            #meta-top-wrapper, #meta-bottom-wrapper  { border-bottom: 1px solid  $border_color_meta_top_css; }
            #meta-top-nav-wrapper .collapse.show ul, #meta-top-nav-wrapper .collapsing ul, #meta-bottom-nav-wrapper .collapse.show ul, #meta-bottom-nav-wrapper .collapsing ul { border: 1px solid  $border_color_meta_top_css; }
            #meta-top-nav-wrapper .collapse.show ul a:hover, #meta-bottom-nav-wrapper .collapse.show ul a:hover { background-color:  $hover_color_meta_top_css_submenu; }
            #meta-top-nav-wrapper nav .navbar-toggler, #meta-bottom-nav-wrapper nav .navbar-toggler { background-color:  $border_color_meta_top_css; border-color: transparent; }
            #meta-top-wrapper .dropdown-menu, #meta-bottom-wrapper .dropdown-menu { background-color:  $background_color_meta_top_css_submenu; }
            #meta-top-wrapper .dropdown-menu, #meta-bottom-wrapper .dropdown-menu { background-size:  $background_size_css_meta_top_submenu; }
            #meta-top-wrapper .dropdown-menu, #meta-bottom-wrapper .dropdown-menu { border: 1px solid  $border_color_meta_top_css_submenu; }
            #meta-top-wrapper .dropdown-menu a:hover, #meta-bottom-wrapper .dropdown-menu a:hover { background-color:  $hover_color_meta_top_css_submenu; }
            #meta-top-search-wrapper { border-left: 1px solid  $border_color_meta_top_css; border-right: 1px solid  $border_color_meta_top_css; }
            #meta-top-search-wrapper form .search_button_wrapper{ border-left: 1px solid  $border_color_meta_top_css; }
            #meta-top-wrapper,  #meta-bottom-wrapper, #meta-top-search-wrapper form .search_button_wrapper .fa, #meta-top-search-wrapper form input[type=\"search\"], #meta-top-search-wrapper form input[type=\"search\"]::placeholder { color:  " . get_theme_mod( 'font_color_meta_top_setting', $this->customizer_defaults["font_color_meta_top_setting"] ) . "; }
            .form-control:focus { border-color: $input_search_border_color_meta_top_css box-shadow: 0 1px 1px $input_search_border_shadow_1_color_meta_top_css inset, 0 0 8px  $input_search_border_shadow_2_color_meta_top_css; }
            #meta-top-wrapper a, #meta-bottom-wrapper a { color:  " . get_theme_mod( 'font_color_meta_top_setting', $this->customizer_defaults["font_color_meta_top_setting"] ) . "; }
            #meta-top-wrapper a:hover, #meta-bottom-wrapper a:hover { color:  " . get_theme_mod( 'font_color_hover_meta_top_setting', $this->customizer_defaults["font_color_hover_meta_top_setting"] ) . "; }
            #meta-main-wrapper { display:  $showhide_metamain_css; }
            #meta-main-wrapper, #meta-main-nav-wrapper .collapse.show ul, #meta-main-nav-wrapper .collapsing ul  { background-color:  $background_color_meta_main_css; }
            #meta-main-wrapper, #meta-main-nav-wrapper .collapse.show ul, #meta-main-nav-wrapper .collapsing ul{ background-size:  $background_size_css_meta_main; }
            #meta-main-wrapper  { border-bottom: 1px solid  $border_color_meta_main_css; }
            #meta-main-nav-wrapper .collapse.show ul, #meta-main-nav-wrapper .collapsing ul { border: 1px solid  $border_color_meta_main_css; }
            #meta-main-nav-wrapper .collapse.show ul a:hover { background-color:  $hover_color_meta_main_css_submenu; }
            #meta-main-nav-wrapper nav .navbar-toggler { background-color:  $border_color_meta_main_css; border-color: transparent; }
            #meta-main-wrapper nav { border-left: 1px solid  $divider_color_meta_main_css }
            #meta-main-wrapper nav .nav-item { border-right: 1px solid  $divider_color_meta_main_css }
            #meta-main-wrapper .dropdown-menu { background-color:  $background_color_meta_main_css_submenu; }
            #meta-main-wrapper .dropdown-menu { background-size:  $background_size_css_meta_main_submenu; }
            #meta-main-wrapper .dropdown-menu { border: 1px solid  $border_color_meta_main_css_submenu; }
            #meta-main-wrapper .dropdown-menu a:hover { background-color:  $hover_color_meta_main_css_submenu; }
            #meta-main-wrapper { color:  " . get_theme_mod( 'font_color_meta_main_setting', $this->customizer_defaults["font_color_meta_main_setting"] ) . "; }
            #meta-main-wrapper a{ color:  " . get_theme_mod( 'font_color_meta_main_setting', $this->customizer_defaults["font_color_meta_main_setting"] ) . "; }
            #meta-main-wrapper a:hover { color:  " . get_theme_mod( 'font_color_hover_meta_main_setting', $this->customizer_defaults["font_color_hover_meta_main_setting"] ) . "; }
            #footer-wrapper { display:  $showhide_footer_css; }
            body { background-color:  $background_color_footer_css; }
            body { background-size:  $background_size_css_footer; }
            #footer-wrapper { color:  " . get_theme_mod( 'font_color_footer_setting', $this->customizer_defaults["font_color_footer_setting"] ) . "; }
            #footer-wrapper a { color:  " . get_theme_mod( 'font_color_link_footer_setting', $this->customizer_defaults["font_color_link_footer_setting"] ) . "; }
            #footer-wrapper a:hover{ color:  " . get_theme_mod( 'font_color_hover_footer_setting', $this->customizer_defaults["font_color_hover_footer_setting"] ) . "; }
            #footer-wrapper .widget ul li { border-top: 1px solid  $border_color_footer_css; }
            #content-wrapper { display:  $showhide_content_css; }
            #content-wrapper { background-color:  $background_color_content_css; }
            #content-wrapper { background-size:  $background_size_css_content; }
            #full-header-wrapper { background-color:  $background_color_full_header_css; }
            #full-header-wrapper { background-size:  $background_size_css_full_header; }
            #content-wrapper { color:  " . get_theme_mod( 'font_color_content_setting', $this->customizer_defaults["font_color_content_setting"] ) . "; }
            #content-wrapper a { color:  " . get_theme_mod( 'font_color_link_content_setting', $this->customizer_defaults["font_color_link_content_setting"] ) . "; }
            #content-wrapper a:hover{ color:  " . get_theme_mod( 'font_color_hover_content_setting', $this->customizer_defaults["font_color_hover_content_setting"] ) . "; }
            #content-wrapper .widget ul li { border-top: 1px solid  $border_color_content_css; }
            #content-wrapper .nav-links span, #content-wrapper .nav-links a { color:  " . get_theme_mod( 'font_color_content_paging_setting', $this->customizer_defaults["font_color_content_paging_setting"] ) . "; }
            #content-wrapper .nav-links a:hover{ color:  " . get_theme_mod( 'font_color_hover_content_paging_setting', $this->customizer_defaults["font_color_hover_content_paging_setting"] ) . "; }
            #content-wrapper .page-numbers.current, #content-wrapper a.page-numbers:hover { background-color:  $background_color_content_paging_css; }
            @media screen and (max-width: " . static::$nav_toggle_break . ") {

                #meta-top-search {
                    background-color:  $background_color_meta_top_css;
                }

            }
            .header-wrapper{ display:  $showhide_header_css; }
            .header-wrapper { background-color:  $background_color_header_css; }
            .header-wrapper { background-size:  $background_size_css_header; }
            .header-wrapper{ border-bottom: 1px solid  $border_color_header_css; }
            .header-wrapper { color:  " . get_theme_mod( 'font_color_header_setting', $this->customizer_defaults["font_color_header_setting"] ) . "; }
            .header-wrapper a { color:  " . get_theme_mod( 'font_color_link_header_setting', $this->customizer_defaults["font_color_link_header_setting"] ) . "; }
            .header-wrapper a:hover{ color:  " . get_theme_mod( 'font_color_hover_header_setting', $this->customizer_defaults["font_color_hover_header_setting"] ) . "; }
    
            .entry-title, .entry-meta { text-align: $header_content_center_content; }
            .page-title { text-align: $header_page_content_center_content; }
            .entry-content, .entry-footer { text-align: $content_content_center_content; }
    
            .article-thumbnail-header { $article_thumbnail_header_show_content; }
            .article-preview-thumbnail { $article_preview_thumbnail_header_show_content; }
            .entry-meta { $article_meta_top_show_content; }
            $article_preview_thumbnail_header_show_extra_content
        </style>\n";

		echo $style;
	}

	/**
	 * Customizer Preview
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function customizer_preview() {

		wp_enqueue_script( 'mcc_customizer_preview', get_template_directory_uri() . '/js/customizer.js', [
			'jquery',
			'customize-preview',
		], static::version, true );

		wp_enqueue_style( "mcc-theme-customizer", get_template_directory_uri() . "/css/customizer.css", [], static::version, 'all' );
	}

	public $customizer_defaults = [

		"background_color_full_header_setting"       => "transparent",
		"background_image_full_header_setting"       => "",
		"background_image_cover_full_header_setting" => true,

		"background_color_meta_top_setting"         => "#424242",
		"background_color_opacity_meta_top_setting" => "1",
		"background_image_meta_top_setting"         => "",
		"background_image_cover_meta_top_setting"   => true,
		"border_color_meta_top_setting"             => "#b2b2b2",
		"border_color_opacity_meta_top_setting"     => "0.15",

		"background_color_meta_main_setting"         => "#ffffff",
		"background_color_opacity_meta_main_setting" => "1",
		"background_image_meta_main_setting"         => "",
		"background_image_cover_meta_main_setting"   => true,
		"border_color_meta_main_setting"             => "#b2b2b2",
		"border_color_opacity_meta_main_setting"     => "0.7",
		"divider_color_meta_main_setting"            => "#969696",
		"divider_color_opacity_meta_main_setting"    => "0.7",

		"background_color_header_setting"         => "#424242",
		"background_color_opacity_header_setting" => "1",
		"background_image_header_setting"         => "",
		"background_image_cover_header_setting"   => true,
		"border_color_header_setting"             => "#b2b2b2",
		"border_color_opacity_header_setting"     => "0.7",

		"background_color_content_setting"       => "#fefefe",
		"background_image_content_setting"       => "",
		"background_image_cover_content_setting" => true,
		"border_color_content_setting"           => "#b2b2b2",
		"border_color_opacity_content_setting"   => "0",

		"background_color_content_paging_setting"         => "#d2d5d7",
		"background_color_content_paging_opacity_setting" => "0.8",

		"background_color_footer_setting"       => "#424242",
		"background_image_footer_setting"       => "",
		"background_image_cover_footer_setting" => true,
		"border_color_footer_setting"           => "#b2b2b2",
		"border_color_opacity_footer_setting"   => "0.15",

		"submenu_keep_first_open_setting" => false,

		"submenu_background_color_meta_top_setting"               => "#424242",
		"submenu_background_color_opacity_meta_top_setting"       => "1",
		"submenu_background_image_meta_top_setting"               => "",
		"submenu_background_image_cover_meta_top_setting"         => true,
		"submenu_border_color_meta_top_setting"                   => "#b2b2b2",
		"submenu_border_color_opacity_meta_top_setting"           => "0.7",
		"submenu_background_hover_color_meta_top_setting"         => "#0166bf",
		"submenu_background_hover_color_opacity_meta_top_setting" => "1",

		"submenu_background_color_meta_main_setting"               => "#ffffff",
		"submenu_background_color_opacity_meta_main_setting"       => "1",
		"submenu_background_image_meta_main_setting"               => "",
		"submenu_background_image_cover_meta_main_setting"         => true,
		"submenu_border_color_meta_main_setting"                   => "#b2b2b2",
		"submenu_border_color_opacity_meta_main_setting"           => "0.15",
		"submenu_background_hover_color_meta_main_setting"         => "#dddddd",
		"submenu_background_hover_color_opacity_meta_main_setting" => "1",

		"showhide_metatop_setting"    => true,
		"showhide_header_setting"     => true,
		"showhide_metamain_setting"   => true,
		"showhide_content_setting"    => true,
		"showhide_metabottom_setting" => true,
		"showhide_footer_setting"     => true,

		"font_color_meta_top_setting"       => "#e5e5e5",
		"font_color_hover_meta_top_setting" => "#77aef4",

		"font_color_header_setting"       => "#e5e5e5",
		"font_color_link_header_setting"  => "#e5e5e5",
		"font_color_hover_header_setting" => "#6b96c6",

		"font_color_meta_main_setting"       => "#848484",
		"font_color_hover_meta_main_setting" => "#0b5da5",

		"font_color_content_setting"              => "#0a0a0a",
		"font_color_link_content_setting"         => "#595959",
		"font_color_hover_content_setting"        => "#0b5da5",
		"font_color_content_paging_setting"       => "#0a0a0a",
		"font_color_hover_content_paging_setting" => "#0f208c",

		"font_color_footer_setting"       => "#aaaaaa",
		"font_color_link_footer_setting"  => "#dbdbdb",
		"font_color_hover_footer_setting" => "#96b5ff",

        "header_center_content_setting" => false,
        "header_page_center_content_setting" => false,
        "content_center_content_setting" => false,
        "article_thumbnail_header_show_setting" => true,
        "article_preview_thumbnail_header_show_setting" => true,
        "article_meta_top_show_setting" => true,

	];

	/**
	 * Customizer
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function customizer_register( $wp_customize ) {

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		// Show/Hide Elements
		$wp_customize->add_section( 'mcc_showhide', [
			'priority'       => 666,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => "Mcc " . __( 'Show/Hide Elements', MCC_THEME_TXT ),
			'description'    => __( 'Show or Hide different Elements<br />(Warning: The elements are hidden only visually. The content is still available in the source text. Search engines can index this content anyway)', MCC_THEME_TXT ),
		] );


        // Article Meta Top Shop
        $wp_customize->add_setting( 'article_meta_top_show_setting', [
            'default'   => $this->customizer_defaults["article_meta_top_show_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'article_meta_top_show_control', [
            'label'    => __( 'Article meta top show?', MCC_THEME_TXT ),
            'section'  => 'mcc_showhide',
            'settings' => 'article_meta_top_show_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );

        // Article Thumbnail Header Show
        $wp_customize->add_setting( 'article_thumbnail_header_show_setting', [
            'default'   => $this->customizer_defaults["article_thumbnail_header_show_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'article_thumbnail_header_show_control', [
            'label'    => __( 'Article thumbnail header show?', MCC_THEME_TXT ),
            'section'  => 'mcc_showhide',
            'settings' => 'article_thumbnail_header_show_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );


        // Article Preview Thumbnail Header Show
        $wp_customize->add_setting( 'article_preview_thumbnail_header_show_setting', [
            'default'   => $this->customizer_defaults["article_preview_thumbnail_header_show_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'article_preview_thumbnail_header_show_control', [
            'label'    => __( 'Article preview thumbnail header show?', MCC_THEME_TXT ),
            'section'  => 'mcc_showhide',
            'settings' => 'article_preview_thumbnail_header_show_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );


        // Meta Top Show/Hide
		$wp_customize->add_setting( 'showhide_metatop_setting', [
			'default'   => $this->customizer_defaults["showhide_metatop_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_metatop_control', [
			'label'    => __( 'Show Meta Top?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_metatop_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Header Show/Hide
		$wp_customize->add_setting( 'showhide_header_setting', [
			'default'   => $this->customizer_defaults["showhide_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_header_control', [
			'label'    => __( 'Show Header?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_header_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Meta Main Show/Hide
		$wp_customize->add_setting( 'showhide_metamain_setting', [
			'default'   => $this->customizer_defaults["showhide_metamain_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_metamain_control', [
			'label'    => __( 'Show Meta Main?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_metamain_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Content Show/Hide
		$wp_customize->add_setting( 'showhide_content_setting', [
			'default'   => $this->customizer_defaults["showhide_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_content_control', [
			'label'    => __( 'Show Content?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_content_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Meta Bottom Show/Hide
		$wp_customize->add_setting( 'showhide_metabottom_setting', [
			'default'   => $this->customizer_defaults["showhide_metabottom_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_metabottom_control', [
			'label'    => __( 'Show Meta Bottom?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_metabottom_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Footer Show/Hide
		$wp_customize->add_setting( 'showhide_footer_setting', [
			'default'   => $this->customizer_defaults["showhide_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'showhide_footer_control', [
			'label'    => __( 'Show Footer?', MCC_THEME_TXT ),
			'section'  => 'mcc_showhide',
			'settings' => 'showhide_footer_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		// Backgrounds + Borders
		$wp_customize->add_panel( 'mcc_backgrounds', [
			'priority'       => 666,
			'capability'     => 'edit_theme_options',
			'theme_supports' => '',
			'title'          => "Mcc " . __( 'Background/Border', MCC_THEME_TXT ),
			'description'    => __( 'Customize Backgrounds', MCC_THEME_TXT ),
		] );

		/////////////////////////////////////////////////////////////////////////////
		// Full Header Section Backgrounds
		$wp_customize->add_section( 'mcc_background_full_header', [
			'title'       => __( 'Full Header', MCC_THEME_TXT ),
			'description' => __( 'Customize Complete Header Background', MCC_THEME_TXT ),
			'priority'    => 00,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );

		// Background Color
		$wp_customize->add_setting( 'background_color_full_header_setting', [
			'default'   => $this->customizer_defaults["background_color_full_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_full_header_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_full_header',
			'settings' => 'background_color_full_header_setting',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_full_header_setting', [
			'default'   => $this->customizer_defaults["background_image_full_header_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_full_header_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_full_header',
			'settings' => 'background_image_full_header_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_full_header_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_full_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_full_header_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_full_header',
			'settings' => 'background_image_cover_full_header_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		// Meta Top+Footer Section Backgrounds + Borders
		$wp_customize->add_section( 'mcc_background_meta_top', [
			'title'       => __( 'Meta Top+Footer', MCC_THEME_TXT ),
			'description' => __( 'Customize Meta Top+Footer Backgrounds and Borders', MCC_THEME_TXT ),
			'priority'    => 10,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );

		// Background Color
		$wp_customize->add_setting( 'background_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["background_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_top_divider_1', [
				'label'    => __( 'Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_top',
				'settings' => 'background_color_meta_top_setting',
			] ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_meta_top_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'background_color_meta_top_setting',
		] ) );

		// Background Color Opacity
		$wp_customize->add_setting( 'background_color_opacity_meta_top_setting', [
			'default'   => $this->customizer_defaults["background_color_opacity_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_color_opacity_meta_top_control', [
			'label'    => __( 'Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'background_color_opacity_meta_top_setting',
			'type'     => 'text',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_meta_top_setting', [
			'default'   => $this->customizer_defaults["background_image_meta_top_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_meta_top_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'background_image_meta_top_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_meta_top_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_meta_top_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'background_image_cover_meta_top_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_top_divider_2', [
				'label'    => __( 'Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_top',
				'settings' => 'background_color_meta_top_setting',
			] ) );

		// Border Color
		$wp_customize->add_setting( 'border_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["border_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color_meta_top_control', [
			'label'    => __( 'Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'border_color_meta_top_setting',
		] ) );

		// Border Color Opacity
		$wp_customize->add_setting( 'border_color_opacity_meta_top_setting', [
			'default'   => $this->customizer_defaults["border_color_opacity_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'border_color_opacity_meta_top_control', [
			'label'    => __( 'Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'border_color_opacity_meta_top_setting',
			'type'     => 'text',
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_top_divider_3', [
				'label'    => __( 'Submenu Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_top',
				'settings' => 'background_color_meta_top_setting',
			] ) );

		// Submenu Keep First Open

		$wp_customize->add_setting( 'submenu_keep_first_open_setting', [
			'default'   => $this->customizer_defaults["submenu_keep_first_open_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_keep_first_open_control', [
			'label'    => __( 'Keep first submenu open', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_keep_first_open_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Submenu Background Color 
		$wp_customize->add_setting( 'submenu_background_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_background_color_meta_top_control', [
			'label'    => __( 'Submenu Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_color_meta_top_setting',
		] ) );

		// Submenu Background Color Opacity
		$wp_customize->add_setting( 'submenu_background_color_opacity_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_color_opacity_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_color_opacity_meta_top_control', [
			'label'    => __( 'Submenu Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_color_opacity_meta_top_setting',
			'type'     => 'text',
		] ) );

		// Submenu Background Image 
		$wp_customize->add_setting( 'submenu_background_image_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_image_meta_top_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'submenu_background_image_meta_top_control', [
			'label'    => __( 'Submenu Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_image_meta_top_setting',
		] ) );

		// Submenu Background Image Cover 
		$wp_customize->add_setting( 'submenu_background_image_cover_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_image_cover_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_image_cover_meta_top_control', [
			'label'    => __( 'Submenu Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_image_cover_meta_top_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		// Submenu Background Hover Color
		$wp_customize->add_setting( 'submenu_background_hover_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_hover_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_background_hover_color_meta_top_control', [
			'label'    => __( 'Submenu Background Hover Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_hover_color_meta_top_setting',
		] ) );

		// Submenu Background Hover Color Opacity
		$wp_customize->add_setting( 'submenu_background_hover_color_opacity_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_background_hover_color_opacity_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_hover_color_opacity_meta_top_control', [
			'label'    => __( 'Submenu Background Hover Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_background_hover_color_opacity_meta_top_setting',
			'type'     => 'text',
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_top_divider_4', [
				'label'    => __( 'Submenu Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_top',
				'settings' => 'background_color_meta_top_setting',
			] ) );

		// Submenu Border Color
		$wp_customize->add_setting( 'submenu_border_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_border_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_border_color_meta_top_control', [
			'label'    => __( 'Submenu Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_border_color_meta_top_setting',
		] ) );

		// Submenu Border Color Opacity
		$wp_customize->add_setting( 'submenu_border_color_opacity_meta_top_setting', [
			'default'   => $this->customizer_defaults["submenu_border_color_opacity_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_border_color_opacity_meta_top_control', [
			'label'    => __( 'Submenu Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_top',
			'settings' => 'submenu_border_color_opacity_meta_top_setting',
			'type'     => 'text',
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		// Header Frontpage/Subpages
		$wp_customize->add_section( 'mcc_background_header', [
			'title'       => __( 'Header', MCC_THEME_TXT ),
			'description' => __( 'Customize Header Backgrounds and Borders', MCC_THEME_TXT ),
			'priority'    => 20,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );

		// Background Color
		$wp_customize->add_setting( 'background_color_header_setting', [
			'default'   => $this->customizer_defaults["background_color_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'header_divider_1', [
				'label'    => __( 'Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_header',
				'settings' => 'background_color_header_setting',
			] ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_header_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'background_color_header_setting',
		] ) );

		// Background Color Opacity
		$wp_customize->add_setting( 'background_color_opacity_header_setting', [
			'default'   => $this->customizer_defaults["background_color_opacity_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_color_opacity_header_control', [
			'label'    => __( 'Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'background_color_opacity_header_setting',
			'type'     => 'text',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_header_setting', [
			'default'   => $this->customizer_defaults["background_image_header_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_header_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'background_image_header_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_header_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_header_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'background_image_cover_header_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'header_divider_2', [
				'label'    => __( 'Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_header',
				'settings' => 'background_color_header_setting',
			] ) );

		// Border Color
		$wp_customize->add_setting( 'border_color_header_setting', [
			'default'   => $this->customizer_defaults["border_color_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color_header_control', [
			'label'    => __( 'Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'border_color_header_setting',
		] ) );

		// Border Color Opacity
		$wp_customize->add_setting( 'border_color_opacity_header_setting', [
			'default'   => $this->customizer_defaults["border_color_opacity_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'border_color_opacity_header_control', [
			'label'    => __( 'Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_header',
			'settings' => 'border_color_opacity_header_setting',
			'type'     => 'text',
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		// Meta Main Section Backgrounds + Borders
		$wp_customize->add_section( 'mcc_background_meta_main', [
			'title'       => __( 'Meta Main', MCC_THEME_TXT ),
			'description' => __( 'Customize Meta Main Backgrounds and Borders', MCC_THEME_TXT ),
			'priority'    => 30,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );

		// Background Color
		$wp_customize->add_setting( 'background_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["background_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_main_divider_1', [
				'label'    => __( 'Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_main',
				'settings' => 'background_color_meta_main_setting',
			] ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_meta_main_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'background_color_meta_main_setting',
		] ) );

		// Background Color Opacity
		$wp_customize->add_setting( 'background_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["background_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_color_opacity_meta_main_control', [
			'label'    => __( 'Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'background_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_meta_main_setting', [
			'default'   => $this->customizer_defaults["background_image_meta_main_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_meta_main_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'background_image_meta_main_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_meta_main_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_meta_main_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'background_image_cover_meta_main_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_main_divider_2', [
				'label'    => __( 'Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_main',
				'settings' => 'background_color_meta_main_setting',
			] ) );

		// Border Color
		$wp_customize->add_setting( 'border_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["border_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color_meta_main_control', [
			'label'    => __( 'Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'border_color_meta_main_setting',
		] ) );

		// Border Color Opacity
		$wp_customize->add_setting( 'border_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["border_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'border_color_opacity_meta_main_control', [
			'label'    => __( 'Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'border_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		// Divider Border Color
		$wp_customize->add_setting( 'divider_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["divider_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'divider_color_meta_main_control', [
			'label'    => __( 'Divider Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'divider_color_meta_main_setting',
		] ) );

		// Divider Color Opacity
		$wp_customize->add_setting( 'divider_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["divider_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'divider_color_opacity_meta_main_control', [
			'label'    => __( 'Divider Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'divider_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_main_divider_3', [
				'label'    => __( 'Submenu Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_main',
				'settings' => 'background_color_meta_main_setting',
			] ) );

		// Submenu Keep First Open

		$wp_customize->add_setting( 'submenu_keep_first_open_setting', [
			'default'   => $this->customizer_defaults["submenu_keep_first_open_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_keep_first_open_control', [
			'label'    => __( 'Keep first submenu open', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_keep_first_open_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'Yes', MCC_THEME_TXT ),
				false => __( 'No', MCC_THEME_TXT ),
			],
		] ) );

		// Submenu Background Color 
		$wp_customize->add_setting( 'submenu_background_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_background_color_meta_main_control', [
			'label'    => __( 'Submenu Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_color_meta_main_setting',
		] ) );

		// Submenu Background Color Opacity
		$wp_customize->add_setting( 'submenu_background_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_color_opacity_meta_main_control', [
			'label'    => __( 'Submenu Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		// Submenu Background Image 
		$wp_customize->add_setting( 'submenu_background_image_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_image_meta_main_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'submenu_background_image_meta_main_control', [
			'label'    => __( 'Submenu Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_image_meta_main_setting',
		] ) );

		// Submenu Background Image Cover 
		$wp_customize->add_setting( 'submenu_background_image_cover_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_image_cover_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_image_cover_meta_main_control', [
			'label'    => __( 'Submenu Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_image_cover_meta_main_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		// Submenu Background Hover Color
		$wp_customize->add_setting( 'submenu_background_hover_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_hover_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_background_hover_color_meta_main_control', [
			'label'    => __( 'Submenu Background Hover Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_hover_color_meta_main_setting',
		] ) );

		// Submenu Background Hover Color Opacity
		$wp_customize->add_setting( 'submenu_background_hover_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_background_hover_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_background_hover_color_opacity_meta_main_control', [
			'label'    => __( 'Submenu Background Hover Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_background_hover_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'meta_main_divider_4', [
				'label'    => __( 'Submenu Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_meta_main',
				'settings' => 'background_color_meta_main_setting',
			] ) );

		// Submenu Border Color
		$wp_customize->add_setting( 'submenu_border_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_border_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'submenu_border_color_meta_main_control', [
			'label'    => __( 'Submenu Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_border_color_meta_main_setting',
		] ) );

		// Submenu Border Color Opacity
		$wp_customize->add_setting( 'submenu_border_color_opacity_meta_main_setting', [
			'default'   => $this->customizer_defaults["submenu_border_color_opacity_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'submenu_border_color_opacity_meta_main_control', [
			'label'    => __( 'Submenu Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_meta_main',
			'settings' => 'submenu_border_color_opacity_meta_main_setting',
			'type'     => 'text',
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		// Footer Section Backgrounds + Borders
		$wp_customize->add_section( 'mcc_background_footer', [
			'title'       => __( 'Footer', MCC_THEME_TXT ),
			'description' => __( 'Customize Footer Backgrounds and Borders', MCC_THEME_TXT ),
			'priority'    => 50,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );

		// Background Color
		$wp_customize->add_setting( 'background_color_footer_setting', [
			'default'   => $this->customizer_defaults["background_color_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_footer_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_footer',
			'settings' => 'background_color_footer_setting',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_footer_setting', [
			'default'   => $this->customizer_defaults["background_image_footer_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_footer_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_footer',
			'settings' => 'background_image_footer_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_footer_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_footer_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_footer',
			'settings' => 'background_image_cover_footer_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		// Border Color
		$wp_customize->add_setting( 'border_color_footer_setting', [
			'default'   => $this->customizer_defaults["border_color_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color_footer_control', [
			'label'    => __( 'Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_footer',
			'settings' => 'border_color_footer_setting',
		] ) );

		// Border Color Opacity
		$wp_customize->add_setting( 'border_color_opacity_footer_setting', [
			'default'   => $this->customizer_defaults["border_color_opacity_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'border_color_opacity_footer_control', [
			'label'    => __( 'Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_footer',
			'settings' => 'border_color_opacity_footer_setting',
			'type'     => 'text',
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		// Content Section Backgrounds + Borders
		$wp_customize->add_section( 'mcc_background_content', [
			'title'       => __( 'Content', MCC_THEME_TXT ),
			'description' => __( 'Customize Content Backgrounds and Borders', MCC_THEME_TXT ),
			'priority'    => 40,
			'capability'  => 'edit_theme_options',
			'panel'       => 'mcc_backgrounds',
		] );





		// Background Color
		$wp_customize->add_setting( 'background_color_content_setting', [
			'default'   => $this->customizer_defaults["background_color_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'content_divider_1', [
				'label'    => __( 'Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_content',
				'settings' => 'background_color_content_setting',
			] ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_content_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'background_color_content_setting',
		] ) );

		// Background Image
		$wp_customize->add_setting( 'background_image_content_setting', [
			'default'   => $this->customizer_defaults["background_image_content_setting"],
			'transport' => 'refresh',
		] );

		$wp_customize->add_control( new WP_Customize_Media_Control( $wp_customize, 'background_image_content_control', [
			'label'    => __( 'Background Image', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'background_image_content_setting',
		] ) );

		// Background Image Cover
		$wp_customize->add_setting( 'background_image_cover_content_setting', [
			'default'   => $this->customizer_defaults["background_image_cover_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_image_cover_content_control', [
			'label'    => __( 'Background Image Cover?', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'background_image_cover_content_setting',
			'type'     => 'radio',
			'choices'  => [
				true  => __( 'On', MCC_THEME_TXT ),
				false => __( 'Off', MCC_THEME_TXT ),
			],
		] ) );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'content_divider_2', [
				'label'    => __( 'Borders', MCC_THEME_TXT ),
				'section'  => 'mcc_background_content',
				'settings' => 'background_color_content_setting',
			] ) );

		// Border Color
		$wp_customize->add_setting( 'border_color_content_setting', [
			'default'   => $this->customizer_defaults["border_color_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'border_color_content_control', [
			'label'    => __( 'Border Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'border_color_content_setting',
		] ) );

		// Border Color Opacity
		$wp_customize->add_setting( 'border_color_opacity_content_setting', [
			'default'   => $this->customizer_defaults["border_color_opacity_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'border_color_opacity_content_control', [
			'label'    => __( 'Border Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'border_color_opacity_content_setting',
			'type'     => 'text',
		] ) );

		// Background Color
		$wp_customize->add_setting( 'background_color_content_paging_setting', [
			'default'   => $this->customizer_defaults["background_color_content_paging_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new Mcc_Customize_Divider_Control( $wp_customize, 'content_paging_divider_3', [
				'label'    => __( 'Paging Backgrounds', MCC_THEME_TXT ),
				'section'  => 'mcc_background_content',
				'settings' => 'background_color_content_paging_setting',
			] ) );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'background_color_content_paging_control', [
			'label'    => __( 'Background Color', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'background_color_content_paging_setting',
		] ) );

		// Background Hover Color Opacity
		$wp_customize->add_setting( 'background_color_content_paging_opacity_setting', [
			'default'   => $this->customizer_defaults["background_color_content_paging_opacity_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'background_color_content_paging_opacity_control', [
			'label'    => __( 'Background Color Opacity', MCC_THEME_TXT ),
			'section'  => 'mcc_background_content',
			'settings' => 'background_color_content_paging_opacity_setting',
			'type'     => 'text',
		] ) );

		/////////////////////////////////////////////////////////////////////////////
		/////////////////////////////////////////////////////////////////////////////
		// Fonts Section
		$wp_customize->add_section( 'mcc_fonts', [
			'title'       => "Mcc " . __( 'Fonts', MCC_THEME_TXT ),
			'description' => __( 'Customize Fonts', MCC_THEME_TXT ),
			'priority'    => 666,
			'capability'  => 'edit_theme_options',
		] );

        // Header Page Content Center
        $wp_customize->add_setting( 'header_page_center_content_setting', [
            'default'   => $this->customizer_defaults["header_page_center_content_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header_pagecenter_content_control', [
            'label'    => __( 'Page title center', MCC_THEME_TXT ),
            'section'  => 'mcc_fonts',
            'settings' => 'header_page_center_content_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );

        // Header Content Center
        $wp_customize->add_setting( 'header_center_content_setting', [
            'default'   => $this->customizer_defaults["header_center_content_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'header_center_content_control', [
            'label'    => __( 'Header Content Center', MCC_THEME_TXT ),
            'section'  => 'mcc_fonts',
            'settings' => 'header_center_content_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );

        // Content: Content Center
        $wp_customize->add_setting( 'content_center_content_setting', [
            'default'   => $this->customizer_defaults["content_center_content_setting"],
            'transport' => 'postMessage',
        ] );

        $wp_customize->add_control( new WP_Customize_Control( $wp_customize, 'content_center_content_control', [
            'label'    => __( 'Content: Content Center', MCC_THEME_TXT ),
            'section'  => 'mcc_fonts',
            'settings' => 'content_center_content_setting',
            'type'     => 'radio',
            'choices'  => [
                true  => __( 'On', MCC_THEME_TXT ),
                false => __( 'Off', MCC_THEME_TXT ),
            ],
        ] ) );


		// Font Color Meta Top+Footer
		$wp_customize->add_setting( 'font_color_meta_top_setting', [
			'default'   => $this->customizer_defaults["font_color_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_meta_top_control', [
			'label'    => __('Meta Top+Footer Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_meta_top_setting',
		] ) );

		// Font Color Hover Meta Top+Footer
		$wp_customize->add_setting( 'font_color_hover_meta_top_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_meta_top_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_meta_top_control', [
			'label'    => __( 'Meta Top+Footer Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_meta_top_setting',
		] ) );

		// Font Color Header
		$wp_customize->add_setting( 'font_color_header_setting', [
			'default'   => $this->customizer_defaults["font_color_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_header_control', [
			'label'    => __( 'Header Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_header_setting',
		] ) );

		// Font Color Link Header
		$wp_customize->add_setting( 'font_color_link_header_setting', [
			'default'   => $this->customizer_defaults["font_color_link_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_link_header_control', [
			'label'    => __( 'Header Font Color Link', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_link_header_setting',
		] ) );

		// Font Color Hover Header
		$wp_customize->add_setting( 'font_color_hover_header_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_header_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_header_control', [
			'label'    => __( 'Header Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_header_setting',
		] ) );

		// Font Color Meta Main
		$wp_customize->add_setting( 'font_color_meta_main_setting', [
			'default'   => $this->customizer_defaults["font_color_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_meta_main_control', [
			'label'    => __( 'Meta Main Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_meta_main_setting',
		] ) );

		// Font Color Hover Meta Main
		$wp_customize->add_setting( 'font_color_hover_meta_main_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_meta_main_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_meta_main_control', [
			'label'    => __( 'Meta Main Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_meta_main_setting',
		] ) );

		// Font Color Content
		$wp_customize->add_setting( 'font_color_content_setting', [
			'default'   => $this->customizer_defaults["font_color_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_content_control', [
			'label'    => __( 'Content Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_content_setting',
		] ) );

		// Font Color Link Content
		$wp_customize->add_setting( 'font_color_link_content_setting', [
			'default'   => $this->customizer_defaults["font_color_link_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_link_content_control', [
			'label'    => __( 'Content Font Color Link', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_link_content_setting',
		] ) );

		// Font Color Hover Content
		$wp_customize->add_setting( 'font_color_hover_content_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_content_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_content_control', [
			'label'    => __( 'Content Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_content_setting',
		] ) );

		// Font Color Content Paging
		$wp_customize->add_setting( 'font_color_content_paging_setting', [
			'default'   => $this->customizer_defaults["font_color_content_paging_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_content_paging_control', [
			'label'    => __( 'Content Paging Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_content_paging_setting',
		] ) );

		// Font Color Hover Content Paging
		$wp_customize->add_setting( 'font_color_hover_content_paging_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_content_paging_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_content_paging_control', [
			'label'    => __( 'Content Paging Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_content_paging_setting',
		] ) );

		// Font Color Footer
		$wp_customize->add_setting( 'font_color_footer_setting', [
			'default'   => $this->customizer_defaults["font_color_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_footer_control', [
			'label'    => __( 'Footer Font Color', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_footer_setting',
		] ) );

		// Font Color Link Footer
		$wp_customize->add_setting( 'font_color_link_footer_setting', [
			'default'   => $this->customizer_defaults["font_color_link_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_link_footer_control', [
			'label'    => __( 'Footer Font Color Link', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_link_footer_setting',
		] ) );

		// Font Color Hover Footer
		$wp_customize->add_setting( 'font_color_hover_footer_setting', [
			'default'   => $this->customizer_defaults["font_color_hover_footer_setting"],
			'transport' => 'postMessage',
		] );

		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'font_color_hover_footer_control', [
			'label'    => __( 'Footer Font Color Hover', MCC_THEME_TXT ),
			'section'  => 'mcc_fonts',
			'settings' => 'font_color_hover_footer_setting',
		] ) );
	}

	/**
	 * Removes width and height attributes from image tags
	 * Thanks to https://wpscholar.com/blog/remove-image-size-attributes-wordpress-responsive/
	 * @since  1.0.0
	 * @access public
	 *
	 * @param string $html
	 *
	 * @return string
	 */
	public function remove_image_size_attributes( $html ) {

		return preg_replace( '/(width|height)="\d*"/', '', $html );
	}

}