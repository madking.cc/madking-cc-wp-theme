<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Funktionsklasse
 * @since 1.0.0
 */
class mcc_functions {

	/**
	 * PHP constructor.
	 * @since  1.0.0
	 * @access public
	 */
	public function __construct() {
	}

	/**
	 * Funktioniert ähnlich wie wp_parse_args, um eine Default Konfiguration mit
	 * der ggf. bestehenden abzugleichen, aber mit Multidimensionalen Arrays
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @param mixed   &$custom  Konfiguration
	 * @param mixed   $default  Konfiguration
	 * @param boolean $do_empty Auch leere Werte übernehmen?
	 *
	 * @return void
	 */
	public static function parse_args_multidim( &$custom, $default, $do_empty = false ) {

		if ( is_array( $default ) ) {
			foreach ( $default as $default_key => $default_value ) {
				if ( is_object( $default[$default_key] ) ) {
					$default[$default_key] = static::convert_object_to_array( $default[$default_key] );
				}

				if ( ! isset( $custom[$default_key] ) || ( $do_empty && isset( $custom[$default_key] ) && empty( $custom[$default_key] ) ) ) {
					$custom[$default_key] = [];
					$custom[$default_key] = $default[$default_key];
				} else {
					if ( is_array( $custom[$default_key] ) && is_array( $default[$default_key] ) ) {
						static::parse_args_multidim( $custom[$default_key], $default[$default_key] );
					}
				}
			}
		}
	}

	/**
	 * Liefert die aktuelle URL
	 * @since  1.0.0
	 * @access public
	 * @return string
	 */
	public function get_current_url() {

		global $wp;
		$current_url = home_url( add_query_arg( [], $wp->request ) );

		return $current_url;
	}

	/**
	 * Wandelt ein Objekt in ein Array um
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @param object $object Das Objekt
	 *
	 * @return array
	 */
	public static function convert_object_to_array( $object ) {

		$array = json_decode( json_encode( $object ), true );

		return $array;
	}

	/**
	 * Leeres Dummy Callback
	 * @since  1.0.0
	 * @access public
	 * @return void
	 */
	public function callback_dummy() {
	}

	/**
	 * Liefert den Slug zu einem Post Typ
	 * @since  1.0.0
	 * @access public
	 *
	 * @param $post_type string Der Post Type
	 *
	 * @return string|boolean
	 */
	public function get_slug( $post_type ) {

		// Thanks to https://wordpress.stackexchange.com/questions/67408/get-custom-post-type-slug-for-an-archive-page
		if ( $post_type ) {
			$post_type_data = get_post_type_object( $post_type );
			$post_type_slug = $post_type_data->rewrite['slug'];

			return $post_type_slug;
		} else {
			return false;
		}
	}

	/**
	 * Liefert die RGB Werte zu einem HEX Farbencode
	 * Danke an: https://stackoverflow.com/a/15202130
	 * @since  1.0.0
	 * @access public
	 * @static
	 *
	 * @param $hex string Der Hex Farbenwert
	 *
	 * @return array
	 */
	static public function get_rgb_from_hex( $hex ) {

		( strlen( $hex ) === 4 ) ? list( $r, $g, $b ) = sscanf( $hex, "#%1x%1x%1x" ) : list( $r, $g, $b ) = sscanf( $hex, "#%2x%2x%2x" );

		return [ "r" => $r, "g" => $g, "b" => $b ];
	}

	/**
	 * Returns an accessibility-friendly link to edit a post or page.
	 * This also gives us a little context about what exactly we're editing
	 * (post or page?) so that users understand a bit more where they are in terms
	 * of the template hierarchy and their content. Helpful when/if the single-page
	 * layout with multiple posts/pages shown gets confusing.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return void
	 */
	static public function edit_link() {

		edit_post_link( sprintf( /* translators: %s: Name of current post */
			__( 'Edit<span class="screen-reader-text"> "%s"</span>', MCC_THEME_TXT ), get_the_title() ), '<span class="edit-link">', '</span>' );
	}



	/**
	 * Prints HTML with meta information for the current post-date/time and author.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return void
	 */
	static public function posted_on() {

		// Get the author name; wrap it in a link.
		$byline = sprintf( /* translators: %s: post author */
			__( 'by %s', MCC_THEME_TXT ), '<span class="author vcard"><a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">' . get_the_author() . '</a></span>' );

		// Finally, let's write all of this to the page.
		//echo '<span class="posted-on">' . static::time() . '</span><span class="byline"> ' . $byline . '</span>';
		echo '<span class="posted-on">' . static::time() . '</span>';
	}

	/**
	 * Gets a nicely formatted string for the published date.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return string
	 */
	static public function time() {

		$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';
		if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
			//$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
			$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time>';
		}

		$time_string = sprintf( $time_string, get_the_date( DATE_W3C ), get_the_date() );

		// Wrap the time string in a link, and preface it with 'Posted on'.
		return sprintf( /* translators: %s: post date */
			__( '<span class="screen-reader-text">Posted on</span> %s', MCC_THEME_TXT ), $time_string );
	}

	/**
	 * Prints HTML with meta information for the categories, tags and comments.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return void
	 */
	static public function entry_footer() {

		/* translators: used between list items, there is a space after the comma */
		$separate_meta = __( ', ', 'mcc' );

		// Get Categories for posts.
		$categories_list = get_the_category_list( $separate_meta );

		// Get Tags for posts.
		$tags_list = get_the_tag_list( '', $separate_meta );

		// We don't want to output .entry-footer if it will be empty, so make sure its not.
		if ( ( ( static::categorized_blog() && $categories_list ) || $tags_list ) || get_edit_post_link() ) {
			echo '<footer class="entry-footer">';

			if ( 'post' === get_post_type() ) {
				if ( ( $categories_list && static::categorized_blog() ) || $tags_list ) {
					echo '<span class="cat-tags-links">';

					// Make sure there's more than one category before displaying.
					if ( $categories_list && static::categorized_blog() ) {
						//echo '<span class="cat-links">' . mcc_get_svg( array( 'icon' => 'folder-open' ) ) . '<span class="screen-reader-text">' . __( 'Categories', MCC_THEME_TXT ) . '</span>' . $categories_list . '</span>';
						echo '<span class="cat-links"><span class="screen-reader-text">' . __( 'Categories', MCC_THEME_TXT ) . '</span>' . $categories_list . '</span>';
					}

					if ( $tags_list && ! is_wp_error( $tags_list ) ) {
						//echo '<span class="tags-links">' . mcc_get_svg( array( 'icon' => 'hashtag' ) ) . '<span class="screen-reader-text">' . __( 'Tags', MCC_THEME_TXT ) . '</span>' . $tags_list . '</span>';
						echo '<span class="tags-links"><span class="screen-reader-text">' . __( 'Tags', MCC_THEME_TXT ) . '</span>' . $tags_list . '</span>';
					}

					echo '</span>';
				}
			}

			static::edit_link();

			echo '</footer> <!-- .entry-footer -->';
		}
	}

	/**
	 * Returns true if a blog has more than 1 category.
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return bool
	 */
	public static function categorized_blog() {

		$category_count = get_transient( 'mcc_categories' );

		if ( false === $category_count ) {
			// Create an array of all the categories that are attached to posts.
			$categories = get_categories( [
				'fields'     => 'ids',
				'hide_empty' => 1,
				// We only need to know if there is more than one category.
				'number'     => 2,
			] );

			// Count the number of categories that are attached to the posts.
			$category_count = count( $categories );

			set_transient( 'mcc_categories', $category_count );
		}

		// Allow viewing case of 0 or 1 categories in post preview.
		if ( is_preview() ) {
			return true;
		}

		return $category_count > 1;
	}

	/**
	 * Returns attr Array for Responsive Class
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return array
	 */
	public static function attr_responsive() {

		return [ 'class' => 'img-fluid' ];
	}

	/**
	 * Returns post thumbnail src
	 * @since  1.0.0
	 * @access public
	 * @static
	 * @return string
	 */
	public static function post_thumbnail_url($post = null, $size = "mcc-featured-image")
	{
		if(is_null($post)) {
			$post = get_post();
		}
		if ( ! $post ) {
			return '';
		}

		$post_thumbnail_id = get_post_thumbnail_id( $post );
		$image_src = wp_get_attachment_image_src($post_thumbnail_id, $size);
		if(isset($image_src[0]))
			return $image_src[0];
		else
			return "";


	}

	/**
	 * Display the archive title based on the queried object.
	 *
	 * @since 4.1.0
	 *
	 * @see get_the_archive_title()
	 *
	 * @param string $before Optional. Content to prepend to the title. Default empty.
	 * @param string $after  Optional. Content to append to the title. Default empty.
	 */
	static public function the_archive_title( $before = '', $after = '' ) {
		$title = static::get_the_archive_title();

		if ( ! empty( $title ) ) {
			echo $before . $title . $after;
		}
	}

	/**
	 * Retrieve the archive title based on the queried object.
	 *
	 * @since 4.1.0
	 *
	 * @return string Archive title.
	 */
	static public function get_the_archive_title() {
		if ( is_category() ) {
			/* translators: Category archive title. 1: Category name */
			$title = single_cat_title( '', false );
		} elseif ( is_tag() ) {
			/* translators: Tag archive title. 1: Tag name */
			$title =  single_tag_title( '', false );
		} elseif ( is_author() ) {
			/* translators: Author archive title. 1: Author name */
			$title = '<span class="vcard">' . get_the_author() . '</span>';
		} elseif ( is_year() ) {
			/* translators: Yearly archive title. 1: Year */
			$title = sprintf( __( 'Year: %s', MCC_THEME_TXT ), get_the_date( _x( 'Y', 'yearly archives date format', MCC_THEME_TXT ) ) );
		} elseif ( is_month() ) {
			/* translators: Monthly archive title. 1: Month name and year */
			$title = sprintf( __( 'Month: %s', MCC_THEME_TXT ), get_the_date( _x( 'F Y', 'monthly archives date format', MCC_THEME_TXT ) ) );
		} elseif ( is_day() ) {
			/* translators: Daily archive title. 1: Date */
			$title = sprintf( __( 'Day: %s', MCC_THEME_TXT ), get_the_date( _x( 'F j, Y', 'daily archives date format', MCC_THEME_TXT ) ) );
		} elseif ( is_tax( 'post_format' ) ) {
			if ( is_tax( 'post_format', 'post-format-aside' ) ) {
				$title = _x( 'Asides', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-gallery' ) ) {
				$title = _x( 'Galleries', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-image' ) ) {
				$title = _x( 'Images', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-video' ) ) {
				$title = _x( 'Videos', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-quote' ) ) {
				$title = _x( 'Quotes', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-link' ) ) {
				$title = _x( 'Links', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-status' ) ) {
				$title = _x( 'Statuses', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-audio' ) ) {
				$title = _x( 'Audio', 'post format archive title', MCC_THEME_TXT );
			} elseif ( is_tax( 'post_format', 'post-format-chat' ) ) {
				$title = _x( 'Chats', 'post format archive title', MCC_THEME_TXT );
			}
		} elseif ( is_post_type_archive() ) {
			/* translators: Post type archive title. 1: Post type name */
			$title = sprintf( __( 'Archives: %s', MCC_THEME_TXT ), post_type_archive_title( '', false ) );
		} elseif ( is_tax() ) {
			$tax = get_taxonomy( get_queried_object()->taxonomy );
			/* translators: Taxonomy term archive title. 1: Taxonomy singular name, 2: Current taxonomy term */
			$title = sprintf( __( '%1$s: %2$s', MCC_THEME_TXT ), $tax->labels->singular_name, single_term_title( '', false ) );
		} else {
			$title = __( 'Archives' );
		}

		/**
		 * Filters the archive title.
		 *
		 * @since 4.1.0
		 *
		 * @param string $title Archive title to be displayed.
		 */
		return apply_filters( 'get_the_archive_title', $title );
	}
}