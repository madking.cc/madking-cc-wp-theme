<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Widget API: WP_Widget_Recent_Posts class
 * @package    WordPress
 * @subpackage Widgets
 * @since      4.4.0
 */

/**
 * Core class used to implement a Recent Posts widget.
 * @since 2.8.0
 * @see   WP_Widget
 */
class mcc_widget_tax_menu extends WP_Widget {

	/**
	 * Sets up a new Recent Posts widget instance.
	 * @since  2.8.0
	 * @access public
	 */
	public function __construct() {

		$widget_ops = [
			'classname'                   => 'mcc_widget_tax_menu',
			'description'                 => __( 'Hierarchical Taxonomy Navigation.' ),
			'customize_selective_refresh' => true,
		];
		parent::__construct( 'mcc-widget-tax-menu', __( 'Taxonomy Navmenu', MCC_THEME_TXT ), $widget_ops );
		$this->alt_option_name = 'mcc_widget_tax_menu_alt';
	}

	/**
	 * Outputs the content for the current Recent Posts widget instance.
	 * @since  2.8.0
	 * @access public
	 *
	 * @param array $args     Display arguments including 'before_title', 'after_title',
	 *                        'before_widget', and 'after_widget'.
	 * @param array $instance Settings for the current Recent Posts widget instance.
	 */
	public function widget( $args, $instance ) {

	    $content = "";

	    if ( ! isset( $args['widget_id'] ) ) {
			$args['widget_id'] = $this->id;
		}

		// Output Frontend

		if(is_archive())
        {

            $id_archive_current = get_queried_object_id();

            $current = get_term($id_archive_current);

	        $terms_args = [
		        'parent' => $id_archive_current,
		        'orderby'   => 'slug',
		        'order'     => 'ASC',
                'taxonomy' => 'category'
	        ];

	        $childs = get_terms( $terms_args);

	        $content .= "<div class='widget-tax-menu-wrapper'><ul class=\"nav nav-pills flex-column\">\n";

	        $id_parent = $current->parent;

	        if(!empty($id_parent))
            {
                $parent = get_term($id_parent);
                if($parent)
                {
	                $content .= "<li class=\"nav-item\">
                                <a class=\"nav-link back-link\" href=\"".site_url()."/".$parent->taxonomy."/".$parent->slug."\">".$parent->name."</a>
                            </li>\n";
                }
            }

	        //<a class=\"nav-link active disabled\" href=\"".site_url()."/".$current->taxonomy."/".$current->slug."\">".$current->name."</a>
	        $content .= "<li class=\"nav-item\">
                                <span class=\"nav-link active disabled\">".$current->name."</span>
                            </li>\n";

	        if($childs)
	            foreach($childs as $child)
                {

	                $content .= "
                            <li class=\"nav-item\">
                                <a class=\"nav-link\" href=\"".site_url()."/".$child->taxonomy."/".$child->slug."\">".$child->name."</a>
                            </li>\n";
                }


	        $content .= "</ul></div>\n";



        }

        echo $content;
	}

	/**
	 * Handles updating the settings for the current Recent Posts widget instance.
	 * @since  2.8.0
	 * @access public
	 *
	 * @param array $new_instance New settings for this instance as input by the user via
	 *                            WP_Widget::form().
	 * @param array $old_instance Old settings for this instance.
	 *
	 * @return array Updated settings to save.
	 */
	public function update( $new_instance, $old_instance ) {
		$instance              = $old_instance;
		return $instance;
	}

	/**
	 * Outputs the settings form for the Recent Posts widget.
	 * @since  2.8.0
	 * @access public
	 *
	 * @param array $instance Current settings.
	 */
	public function form( $instance ) {

		?>
        <p><?php __( 'This Widget has no settings', MCC_THEME_TXT ); ?>
        </p>

		<?php
	}
}
