<?php

if ( is_active_sidebar( 'mcc-footer-1' ) ) {
	ob_start();
	dynamic_sidebar( 'mcc-footer-1' );
	$sidebar = ob_get_clean();
	if ($sidebar) {
		echo "<div class='col' id='footer-sidebar-1'>";
		echo $sidebar;
		echo "</div>";
	}
}
if ( is_active_sidebar( 'mcc-footer-2' ) ) {
	ob_start();
	dynamic_sidebar( 'mcc-footer-2' );
	$sidebar = ob_get_clean();
	if ($sidebar) {
		echo "<div class='col' id='footer-sidebar-2'>";
		echo $sidebar;
		echo "</div>";
	}
}
if ( is_active_sidebar( 'mcc-footer-3' ) ) {
	ob_start();
	dynamic_sidebar( 'mcc-footer-3' );
	$sidebar = ob_get_clean();
	if ($sidebar) {
		echo "<div class='col' id='footer-sidebar-3'>";
		echo $sidebar;
		echo "</div>";
	}
}