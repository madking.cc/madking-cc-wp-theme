<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

get_header();
get_header("mcc-frontpage");
?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main" role="main">

			<?php // Show the selected frontpage content.
            if ( have_posts() ) :
                $mcc_have_posts = true;
				while ( have_posts() ) : the_post();
					get_template_part( 'templates/page/content', 'front-page' );
				endwhile;
			else : // I'm not sure it's possible to have no posts when this page is shown, but WTH.
				get_template_part( 'templates/post/content', 'none' );
			endif; ?>

		</main><!-- #main -->
	</div><!-- #primary -->

<?php

get_footer("mcc");
get_footer();