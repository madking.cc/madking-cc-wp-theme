<?php
/**
 * Template for displaying search forms in Twenty Seventeen
 * @package    WordPress
 * @subpackage Twenty_Seventeen
 * @since      1.0
 * @version    1.0
 */

?>

<?php $unique_id = esc_attr( uniqid( 'search-form-' ) ); ?>

<form class="form-inline my-2 my-lg-0" role="search" method="get"
      action="<?php

      // Polylang Support (https://polylang.wordpress.com/documentation/documentation-for-developers/functions-reference/)
      if ( function_exists( "pll_current_language" ) ) {
	      $lang_dir = pll_current_language();
	      if ( ! empty( $lang_dir ) ) {
		      $lang_dir .= "/";
	      }

	      echo home_url( '/' ) . $lang_dir;
      } else {
	      echo home_url( '/' );
      }

      ?>">
    <input class="form-control" id="<?php echo $unique_id; ?>" type="search"
           placeholder="<?php echo esc_attr_x( 'Search &hellip;', 'placeholder', MCC_THEME_TXT ); ?>"
           value="<?php echo get_search_query(); ?>" name="s" aria-label="Search">
    <div class="search_button_wrapper">
        <i class="fa fa-search"></i>
        <input class="form-control" type="submit" value="">
    </div>
</form>
