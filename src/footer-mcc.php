<?php
/*
  This program is free software; you can redistribute it and/or
  modify it under the terms of the GNU General Public License
  as published by the Free Software Foundation; either version 2
  of the License, or (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program; if not, write to the Free Software
  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 */

// Security
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}



?>
</div>
<?php get_sidebar(); ?>
</div>
<?php get_template_part( 'templates/nav/nav', 'posts' ); ?>
</div>
</div>
<?php

get_template_part( 'templates/meta/menu', 'bottom' );

$widget_footer = false;
if ( is_active_sidebar( 'mcc-footer-1' ) || is_active_sidebar( 'mcc-footer-2' ) || is_active_sidebar( 'mcc-footer-3' ) ) {
	$widget_footer = true;
}

?>

<div id="footer-wrapper">
    <div class="container">
		<?php
		if ( $widget_footer ) {
			?>

            <div class="row">

				<?php

				get_sidebar( "footer" );

				?>
            </div>
			<?php
		}

		$current_year = date_i18n( "Y" );
		$title        = get_bloginfo( "name" );
		?>
        <div id="footer-bottom">
            <p class="text-center"><?php echo $title." ".__( "©", MCC_THEME_TXT )." ".$current_year.". "; ?>
				<?php echo __( "All rights reserved.", MCC_THEME_TXT ); ?></p>
        </div>

    </div>
</div>
